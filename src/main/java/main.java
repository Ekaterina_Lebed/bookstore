
public class main {
    public static void main(String param){
        if (System.getProperty("com.sun.management.jmxremote") != null) {
            String portString = System.getProperty("com.sun.management.jmxremote.port");
            if (portString != null) {
                System.out.println("JMX running on port " + Integer.parseInt(portString));
            }
        }
        else {
            System.out.println("JMX remote is disabled");
        }
    }
}
