package com.lea.books.enums;


public enum PaymentMethod {
    CASH_AND_DELIEVERY(1, "Наличными - оплата при получении"),
    CART_PAYMENT(2, "Безналичный расчет");

    private int code;
    private String nameRu;

    PaymentMethod(int code, String nameRu) {
        this.code = code;
        this.nameRu = nameRu;
    }

    public int getId(){
        return ordinal();
    }

    public int getCode() {
        return code;
    }

    public String getName(){
        return nameRu;
    }

}
