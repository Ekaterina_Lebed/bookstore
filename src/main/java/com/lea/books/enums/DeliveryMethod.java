package com.lea.books.enums;


public enum DeliveryMethod {
    SELF_SERVICE(1, "Самовывоз (Киев)"),
    COURIER_R1(2, "Курьером по Киеву"),
    COURIER_R2(3, "Курьером по Украине"),
    SERVICE_DELIVERY_D1(4,"Доставка Новой Почтой"),
    SERVICE_DELIVERY_D2(5,"Доставка Автолюксом"),
    SERVICE_DELIVERY_D3(6,"Доставка Укрпочтой");

    private int code;
    private String nameRu;

    DeliveryMethod(int code, String nameRu) {
        this.code = code;
        this.nameRu = nameRu;
    }

    public int getId(){
        return ordinal();
    }

    public int getCode() {
        return code;
    }

    public String getName(){
        return nameRu;
    }

}
