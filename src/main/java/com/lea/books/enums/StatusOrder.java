package com.lea.books.enums;


public enum StatusOrder {
    NEW(1, "Новый"),
    COMPLETED(2, "Выполнен" ),
    CANCELED(3, "Отменен");

    private int code;
    String nameRu;

    StatusOrder(int code, String nameRu) {
        this.code = code;
        this.nameRu = nameRu;
    }

    public int getId(){
        return ordinal();
    }

    public int getCode() {
        return code;
    }

    public String getName(){
        return nameRu;
    }
}
