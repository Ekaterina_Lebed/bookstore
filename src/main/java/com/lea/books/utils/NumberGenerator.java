package com.lea.books.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class NumberGenerator {
    public static String getRandomSymbols(int length){
        if(length<=0) return "";

        char[] chars = "123456789123456789123456789123456789123456789".toCharArray();
        char[] chars2 = "ABCDEFGHAJKLMNAPARSTUVWXYZABCDEFGHAJKLMNAPARSTUVWXYZABCDEFGHAJKLMNAPARSTUVWXYZ".toCharArray();

        char[] text = new char[length];
        Random random = new Random();

        for (int i = 0; i < length; i++) {
            if (i < (length - 2)) {
                text[i] = chars[random.nextInt(chars.length)];
            } else {
                text[i] = chars2[random.nextInt(chars2.length)];
            }
        }
        return new String(text);
    }

    public static String getStringNumber(int length){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR)%100;
        int week = calendar.getWeeksInWeekYear();

        String number = new Integer(year).toString()+new Integer(week).toString();
        number = number + getRandomSymbols(length-number.length());
        return number;
    }
}
