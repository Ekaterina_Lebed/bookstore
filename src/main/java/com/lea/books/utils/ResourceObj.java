package com.lea.books.utils;

public interface ResourceObj {
    byte[] getBFile();
    String getName();
    String getLink();
    void setLink(String link);
}
