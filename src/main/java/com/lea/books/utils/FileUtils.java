package com.lea.books.utils;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class FileUtils {
    private static final Logger LOGGER = Logger.getLogger(FileUtils.class);

    private static boolean createFile(File file){
        try {
            return file.createNewFile();
        } catch (IOException e) {
            LOGGER.error(e);
        }

        return false;
    }

    public static void writeByte(byte[] bFile, File file){
        if(!file.exists() && !createFile(file)) {
            return;
        }

        try{
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bFile);
            fos.close();
        }catch(Exception e){
            LOGGER.error(e);
        }
    }

    public static byte[] readByte(File file){
        byte[] bFile = new byte[(int) file.length()];

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        } catch (Exception e) {
            LOGGER.error(e);
        }

        return bFile;
    }
}
