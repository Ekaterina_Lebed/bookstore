package com.lea.books.utils;

import java.io.File;
import java.util.List;

import org.apache.log4j.Logger;

public class ResourceUtils {
    private static final Logger logger = Logger.getLogger(ResourceUtils.class);

    private String location;
    private String mapping;

    private final Logger LOGGER = Logger.getLogger(ResourceUtils.class);

    public void setLocation(String location) {
        this.location = location;
    }

    public void setMapping(String mapping) {
        this.mapping = mapping;
    }

    private boolean makeLocationIfNotExists(){
        File fileLocation = new File(location);
        if(fileLocation.exists()) {
            LOGGER.debug("Resource directory exists: " + location);
            return true;
        }

        if(fileLocation.mkdirs()) {
            LOGGER.debug("Create new resource directory: " + location);
            return true;
        }

        LOGGER.error("Can not create new resource directory: " + location);
        return false;
    }

    private String makeLink(File file){
        return mapping + file.getName();
    }

    private File saveToFile(ResourceObj rc) {
        File file = new File(location + rc.getName());
        logger.debug("invoked:"+file.getPath());

        if(!makeLocationIfNotExists()){
            LOGGER.error("Can not save image to file"+file.getPath()+", location "+location+" not exists");
            return file;
        }

        if(!file.exists()) {
            FileUtils.writeByte(rc.getBFile(), file);
            LOGGER.debug("Retrieve image to file: " + file.getPath());
        }

        return file;
    }

    public void makeResources(ResourceObj obj){
        if(obj!=null && (obj.getLink()==null || obj.getLink().isEmpty())){
            logger.debug("invoked:"+obj+":"+obj.getLink());
            File file = saveToFile(obj);
            obj.setLink(makeLink(file));
        }
    }

    public void makeResources(List<ResourceObj> objList){
        logger.debug("invoked");
        for (ResourceObj obj:objList){
            if(obj!=null){
                makeResources(obj);
            }
        }
    }
}
