package com.lea.books.service.ui;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.entity.catalog.Section;
import com.lea.books.model.ui.Breadcrumb;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BreadcrumbsBuilder {
    private Map<String,String> mapLinkToPageName =new HashMap<>();

    public List<Breadcrumb> build(Book book){
        return build(book.getSection());
    }

    public List<Breadcrumb> build(Category category){
        List<Breadcrumb> breadcrumbs = new ArrayList<>();
        breadcrumbs.add(getCatalog());

        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.setName(category.getName());
        breadcrumb.setLink("/category?id="+category.getId());

        return breadcrumbs;
    }

    public List<Breadcrumb> build(Section section){
        List<Breadcrumb> breadcrumbs= new ArrayList<>();
        breadcrumbs.add(getCatalog());

        List<Section> path = getPath(section);

        for(Section item : path){
            Breadcrumb breadcrumb = new Breadcrumb();
            breadcrumb.setName(item.getName());
            breadcrumb.setLink("/section?id="+item.getId());
            breadcrumbs.add(breadcrumb);
        }

        return breadcrumbs;
    }

    public List<Breadcrumb> build(){
        List<Breadcrumb> breadcrumbs= new ArrayList<>();
        breadcrumbs.add(getCatalog());

        return breadcrumbs;
    }

    public void setMapLinkToPageName(Map<String, String> map) {
        this.mapLinkToPageName.clear();
        this.mapLinkToPageName.putAll(map);
    }

    public List<Breadcrumb> build(String link){
        List<Breadcrumb> breadcrumbs = new ArrayList<>();

        String name= mapLinkToPageName.get(link);
        breadcrumbs.add(new Breadcrumb(name,link));

        return breadcrumbs;
    }

    private List<Section> getPath(Section section){
        List<Section> path = new ArrayList<>();
        Section item = section;

        while (item != null){
            path.add(item);
            item = item.getRoot();
        }

        Collections.reverse(path);
        return path;
    }

    private Breadcrumb getCatalog(){
        Breadcrumb breadcrumb = new Breadcrumb();
        breadcrumb.setName("Каталог");
        breadcrumb.setLink("/catalog");
        return breadcrumb;
    }
}
