package com.lea.books.service.user;

import com.lea.books.dao.api.UserDao;
import com.lea.books.dao.api.UserRoleDao;
import com.lea.books.model.common.Contact;
import com.lea.books.entity.user.User;
import com.lea.books.service.system.SecurityService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);

    @Autowired
    UserDao userDao;

    @Autowired
    UserRoleDao userRoleDao;

    @Autowired
    SecurityService securityService;

    public int insert(User item) {
        int status = 0;

        try {
            userDao.insert(item);
            logger.debug("insert: " + item);
        } catch (Exception e) {
            status = 1;
            logger.error("error code = " + status, e);
        }
        return status;
    }

    public User getCurrentUser() {
        User item = null;
        try {
            item = userDao.selectByLogin(securityService.getUserName());
        } catch (Exception e) {
            logger.error(null, e);
        }
        return item;
    }

    public void update(User item) {
        try {
            userDao.update(item);
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    public void updateContact(User user, Contact contact) {
        if (!(user != null && contact != null)) {
            return;
        }
        user.setPhone(contact.getPhone());
        user.setEmail(contact.getEmail());
        user.setCity(contact.getCity());
        user.setStreet(contact.getStreet());
        user.setHouse(contact.getHouse());
        user.setFlat(contact.getFlat());
        update(user);
    }

    public Contact getContact(User user) {
        Contact contact = new Contact();
        contact.setEmail(user.getEmail());
        contact.setPhone(user.getPhone());
        contact.setCity(user.getCity());
        contact.setStreet(user.getStreet());
        contact.setHouse(user.getHouse());
        contact.setFlat(user.getFlat());

        return contact;
    }
}