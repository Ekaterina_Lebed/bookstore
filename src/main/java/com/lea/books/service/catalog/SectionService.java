package com.lea.books.service.catalog;

import com.lea.books.dao.api.SectionDao;
import com.lea.books.entity.catalog.Section;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionService {
    @Autowired
    SectionDao sectionDao;

    private static final Logger logger = Logger.getLogger(SectionService.class);

    public void insert(Section item){
        try {
            sectionDao.insert(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

    public void update(Section item){
        try {
            sectionDao.update(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

    public Section selectSectionById(int id){
        return sectionDao.selectById(id);
    }

    public List <Section> selectByParent(Section parent){
        return sectionDao.selectByParent(parent);
    }

}
