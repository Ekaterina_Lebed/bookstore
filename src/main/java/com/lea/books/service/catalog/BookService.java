package com.lea.books.service.catalog;

import com.lea.books.dao.api.BookDao;
import com.lea.books.dao.api.SectionDao;
import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.entity.catalog.Section;
import com.lea.books.model.catalog.BookSet;
import com.lea.books.utils.ResourceObj;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {
    @Autowired
    BookDao bookDao;

    @Autowired
    SectionDao sectionDao;

    private static final Logger logger = Logger.getLogger(BookService.class);

    public void insert(Book item){
        try {
            bookDao.insert(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

    public void update(Book item){
        try {
            bookDao.update(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

    public List<Book> selectByCategory(Category category,int count){
        return bookDao.selectByCategory(category,count);
    }

    public List<Book> selectByCategory(Category category){
        return bookDao.selectByCategory(category);
    }

    public List<Book> selectBySection(Section section){
        List<Section>sections = sectionDao.selectBySection(section);
        return bookDao.selectBySections(sections);
    }

    public List<Book> search(String name){
        return bookDao.selectByName(name);
    }

    public Book selectById(int id){
        return bookDao.selectById(id);
    }

    public List<BookSet> buildBookSets(List<Category> categoryList,int count){
        List<BookSet>bookSets = new ArrayList<>();

        for (Category category : categoryList){
            BookSet item = new BookSet();
            item.setCategory(category);
            item.setBooks(bookDao.selectByCategory(category,count));
            bookSets.add(item);
        }
        return bookSets;
    }

    public ResourceObj getResources(Book book){
        logger.debug("invoked");
        return book.getImage();
    }

    public List<ResourceObj> getResources(List<Book> books){
        List<ResourceObj> list = new ArrayList<>();
        for (Book book:books){
            list.add(book.getImage());
        }
        return list;
    }

    public List<ResourceObj> getResourcesList(List<BookSet> sets){
        List<ResourceObj> list = new ArrayList<>();
        for (BookSet set:sets){
            list.addAll(getResources(set.getBooks()));
        }
        return list;
    }
}
