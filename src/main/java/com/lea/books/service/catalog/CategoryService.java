package com.lea.books.service.catalog;

import com.lea.books.dao.api.CategoryDao;
import com.lea.books.entity.catalog.Category;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    @Autowired
    CategoryDao categoryDao;

    @Autowired
    Category noveltyCategory;

    @Autowired
    Category topSalesCategory;

    private static final Logger logger = Logger.getLogger(CategoryService.class);

    public void insert(Category item){
        try {
            categoryDao.insert(item);
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    public void update(Category item){
        try {
            categoryDao.update(item);
        } catch (Exception e) {
            logger.error(null, e);
        }
    }

    public Category selectById(int id) {
        return categoryDao.selectById(id);
    }

    public Category noveltyCategory(){
        return  noveltyCategory;
    }

    public Category topSalesCategory(){
        return topSalesCategory;
    }
}
