package com.lea.books.service.catalog;

import com.lea.books.model.catalog.CatalogBranch;
import com.lea.books.entity.catalog.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class CatalogService {
    @Autowired
    SectionService sectionService;

    public List<CatalogBranch> buildBranches(int deep){
        return buildBranches(deep, null);
    }

    public List<CatalogBranch> buildBranches(int deep,Section parent){
        List<CatalogBranch> branches = new ArrayList<>();

        List<Section> sections = sort(sectionService.selectByParent(parent));

        for(Section section : sections){
            branches.add(buildBranch(deep, section));
        }

        return branches;
    }

    private CatalogBranch buildBranch(final int deep,Section section){
        CatalogBranch branch = new CatalogBranch();
        branch.setSection(section);

        if((deep-1) > 0) {
            List<Section> subSections = sort(sectionService.selectByParent(section));

            for (Section subSection : subSections) {
                branch.addSubBranch(buildBranch(deep-1, subSection));
            }
        }

        return branch;
    }

    private List<Section> sort(List<Section> sections){
        Comparator<Section> comparator = new Comparator<Section>(){
            @Override
            public int compare(Section o1, Section o2) {
                return o1.getName().compareTo(o2.getName());
            }
        };

        Collections.sort(sections, comparator);
        return  sections;
    }
}
