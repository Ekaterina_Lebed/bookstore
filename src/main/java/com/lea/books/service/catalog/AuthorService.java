package com.lea.books.service.catalog;

import com.lea.books.dao.api.AuthorDao;
import com.lea.books.entity.catalog.Author;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthorService {
    @Autowired
    AuthorDao authorDao;

    private static final Logger logger = Logger.getLogger(AuthorService.class);

    public void insert(Author item){
        try {
            authorDao.insert(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

    public void update(Author item){
        try {
            authorDao.update(item);
        } catch (Exception e) {
            logger.error(null,e);
        }
    }

}
