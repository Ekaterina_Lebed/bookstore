package com.lea.books.service.order;


import com.lea.books.dao.api.OrderDao;
import com.lea.books.enums.*;
import com.lea.books.entity.order.Order;
import com.lea.books.entity.order.OrderItem;
import com.lea.books.model.common.Contact;
import com.lea.books.entity.user.User;
import com.lea.books.model.cart.CartItem;
import com.lea.books.utils.NumberGenerator;
import com.lea.books.utils.ResourceObj;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderService {
    private static final Logger logger = Logger.getLogger(OrderService.class);

    @Autowired
    OrderDao orderDao;

    public int insert(Order item){
        int status = 0;

        try {
            orderDao.insert(item);
            logger.debug("Order: " + item);
        } catch (Exception e) {
            status = 1;
            logger.error("error code = "+status, e);
        }

        return status;
    }

    public Order selectById(int id){
        Order order = null;
        try {
            if(logger.isDebugEnabled())
                logger.debug("ID=" + id);

            order = orderDao.selectById(id);
        } catch (Exception e) {
            logger.error("id=" + id, e);
        }
        return order;
    }

    public List<Order> selectByUser(User user){
        List<Order> list = null;
        try {
            list = orderDao.selectByUser(user);
            logger.debug("select order list, user = " + user);
        } catch (Exception e) {
            logger.error(null, e);
        }

        return list;
    }

    public List<Order> selectAll(){
        List<Order> list = null;
        try {
            list = orderDao.selectAll();
            logger.debug("select order list ");
        } catch (Exception e) {
            logger.error(null, e);
        }
        return list;
    }

    public void updateContact(Order order, Contact contact){
        order.setPhone(contact.getPhone());
        order.setEmail(contact.getEmail());
        order.setCity(contact.getCity());
        order.setStreet(contact.getStreet());
        order.setHouse(contact.getHouse());
        order.setFlat(contact.getFlat());
    }

    public void updateItems(Order order, List<CartItem> items){
        List<OrderItem> orderItems = new ArrayList<>();
        for(CartItem cartItem : items){
            OrderItem orderItem = new OrderItem();
            orderItem.setOrder(order);
            updateItem(orderItem, cartItem);

            orderItems.add(orderItem);
        }

        order.setItems(orderItems);
        updateTotalSum(order);
    }

    private void updateItem(OrderItem orderItem, CartItem cartItem){
        orderItem.setBook(cartItem.getBook());
        orderItem.setPrice(cartItem.getPrice());
        orderItem.setCount(cartItem.getCount());
        orderItem.setSum(cartItem.getSum());
    }

    private void updateTotalSum(Order order){
        double sum = 0;
        for(OrderItem item:order.getItems()) {
            sum += item.getSum();
        }
        order.setSum(sum);
    }

    public Order createNew(){
        Order order = new Order();
        order.setDate(new Date());
        order.setStatus(StatusOrder.NEW);
        order.setNumber(NumberGenerator.getStringNumber(8));
        return order;
    }

    public List<ResourceObj> getResources(Order order) {
        List<ResourceObj> list = new ArrayList<>();
        for (OrderItem item:order.getItems()){
            list.add(item.getBook().getImage());
        }
        return list;
    }
}
