package com.lea.books.service.cart;

import com.lea.books.entity.catalog.Book;
import com.lea.books.model.cart.Cart;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class CartService {
    private static final Logger logger = Logger.getLogger(CartService.class);

    public void addCartItem(Cart cart, Book book, int count){
        if(book == null) {
            logger.debug("ignored empty: book = null");
            return;
        }

        cart.addItem(book, count);
        logger.debug("update cart item: book = " + book.getId() + ", count = " + count);
    }

    public void updateCartItem(Cart cart, Book book, int count){
        if(book == null) {
            logger.debug("ignored empty: book = null");
            return;
        }

        if(count > 0){
            cart.updateItem(book, count);
            logger.debug("update cart item: book = " + book.getId() + ", count = " + count);
        }
        else {
            cart.deleteItem(book.getId());
            logger.debug("delete cart item: book = "+book.getId() + ", count = " + count);
        }

    }

    public void deleteCartItem(Cart cart, int id){
        if(id > 0){
            cart.deleteItem(id);
            logger.debug("update cart item: book = " + id);
        }
        else {
            logger.debug("ignored empty: id = " + id);
        }

    }

}
