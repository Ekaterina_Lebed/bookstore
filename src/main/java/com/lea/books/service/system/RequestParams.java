package com.lea.books.service.system;

import com.lea.books.enums.DeliveryMethod;
import com.lea.books.enums.PaymentMethod;
import com.lea.books.model.common.Contact;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class RequestParams {
    private static final Logger logger = Logger.getLogger(RequestService.class);
    private HttpServletRequest request;

    public RequestParams(HttpServletRequest request) {
        this.request = request;
    }

    public Contact getContact(){
        Contact contact = new Contact();
        contact.setPhone(getString("orderPhone"));
        contact.setEmail(getString("orderEmail"));
        contact.setCity(getString("orderCity"));
        contact.setStreet(getString("orderStreet"));
        contact.setHouse(getString("orderHouse"));
        contact.setFlat(getString("orderFlat"));

        return contact;
    }

    public DeliveryMethod getDelivery(String s) {
        int id = getInteger(s);
        return DeliveryMethod.values()[id];
    }

    public PaymentMethod getPayment(String s) {
        int id = getInteger(s);
        return PaymentMethod.values()[id];
    }

    public int getInteger(String s){
        if(s==null)return 0;

        int param = 0;
        if(s.isEmpty()) {
            logger.error("Ignore empty parameter name, return param = " + param);
            return param;
        }

        String _param = request.getParameter(s);
        try {
            param = Integer.parseInt(_param);
        }catch (NumberFormatException e){
            logger.error(s + " (" + _param + ") is not a numerical value",e);
        }

        return param;
    }

    public String getString(String s){
        return request.getParameter(s);
    }
}
