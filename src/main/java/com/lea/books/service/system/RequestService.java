package com.lea.books.service.system;

import com.lea.books.model.cart.Cart;
import com.lea.books.model.cart.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Service
public class RequestService {
    @Autowired
    Currency currency;

    private Cart getShoppingCart(HttpSession session){
        Cart cart = (Cart)session.getAttribute("cart");
        if (cart == null){
            cart = new Cart();
            cart.setCurrency(currency);
            session.setAttribute("cart",cart);
        }
        return cart;
    }

    public Cart getCart(HttpServletRequest request){
        HttpSession session = request.getSession(true);
        return getShoppingCart(session);
    }
}
