package com.lea.books.model.catalog;
import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;

import java.util.List;

public class BookSet {

    private List<Book> books;
    private Category category;

    public Category getGroup() {
        return category;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
