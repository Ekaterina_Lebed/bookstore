package com.lea.books.model.catalog;


import com.lea.books.entity.catalog.Section;

import java.util.ArrayList;
import java.util.List;

public class CatalogBranch{
    private Section section;
    private List<CatalogBranch> subBranches = new ArrayList<>();
    private int weight;

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
        updateWeight();
    }

    public void addSubBranch(CatalogBranch branch){
        subBranches.add(branch);
        updateWeight();
    }

    public int getWeight() {
        return weight;
    }

    private void updateWeight(){
        weight = getWeight(section) + getWeight(subBranches);
    }

    private int getWeight(Section section){
        int weight = section.getName().length();
        return 1 + (weight - weight%10) / 10;
    }

    private int getWeight(List<CatalogBranch> branches){
        int weight = 0;
        for (CatalogBranch branchItem:branches) {
            weight += branchItem.getWeight();
        }
        return weight;
    }

    public List<CatalogBranch> getSubBranches() {
        return subBranches;
    }
}
