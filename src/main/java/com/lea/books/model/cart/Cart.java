package com.lea.books.model.cart;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lea.books.entity.catalog.Book;
import com.lea.books.utils.ResourceObj;
import org.apache.log4j.Logger;

import java.util.*;

public class Cart {
    private static final Logger logger = Logger.getLogger(Cart.class);

    private double sum;
    private int count;
    private Currency currency;
    private Map<Integer,CartItem> cartItems = new LinkedHashMap<>();

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public List<ResourceObj> getResources() {
        List<ResourceObj> list = new ArrayList<>();

        for (CartItem item:cartItems.values()){
            list.add(item.getBook().getImage());
        }

        return list;
    }

    synchronized public int getCount(){
        return count;
    }

    synchronized public double getSum() {
        return sum;
    }

    synchronized public boolean isEmpty(){
        return cartItems.isEmpty();
    }

    synchronized public void clear(){
        logger.debug("delete all cart items");
        cartItems.clear();
        updateTotalValues();
    }

    synchronized public String toJson(){
        final JsonObject currencyValue = new JsonObject();
        currencyValue.addProperty("name",currency.getName());

        final JsonArray cartItemsValue = new JsonArray();
        Collection<CartItem>items = cartItems.values();

        for (CartItem cartItem : items){
            JsonObject bookValue = new JsonObject();
            if(cartItem.getBook()!=null){
                bookValue.addProperty("id", cartItem.getBook().getId());
                bookValue.addProperty("name", cartItem.getBook().getName());
                bookValue.addProperty("article", cartItem.getBook().getArticle());
            }

            JsonObject itemValue = new JsonObject();
            itemValue.addProperty("id", cartItem.getId());
            itemValue.add("book", bookValue);
            itemValue.addProperty("count", cartItem.getCount());
            itemValue.addProperty("price", cartItem.getPrice());
            itemValue.addProperty("sum",   cartItem.getSum());
            cartItemsValue.add(itemValue);
        }

        JsonObject result = new JsonObject();
        result.addProperty("sum",sum);
        result.addProperty("count",count);
        result.add("currency", currencyValue);
        result.add("cartItems", cartItemsValue);

        return result.toString();
    }

    synchronized public void addItem(Book book, int count){
        int id = book.getId();
        logger.debug("book.id = " + id);

        CartItem newItem = new CartItem();
        newItem.setId(book.getId());
        newItem.setBook(book);
        newItem.updatePrice(book.getPrice());
        newItem.updateQuantity(count);

        CartItem curItem = cartItems.get(id);
        if(curItem != null){
            newItem.updatePrice(curItem.getPrice());
            newItem.updateQuantity(newItem.getCount() + curItem.getCount());
        }

        setItem(id, newItem);
        updateTotalValues();
    }

    synchronized public void updateItem(Book book, int count) {
        int id = book.getId();
        logger.debug("book.id = " + id);

        CartItem newItem = new CartItem();
        newItem.setId(book.getId());
        newItem.setBook(book);
        newItem.updatePrice(book.getPrice());
        newItem.updateQuantity(count);

        CartItem curItem = cartItems.get(id);
        if(curItem != null){
            newItem.updatePrice(curItem.getPrice());
        }

        setItem(id, newItem);
        updateTotalValues();
    }

    synchronized public void deleteItem(int bookId) {
        logger.debug("book.id = " + bookId);

        CartItem curItem = cartItems.get(bookId);
        deleteItem(bookId, curItem);
        updateTotalValues();
    }

    synchronized public List<CartItem> getItems(){
        List<CartItem> items = new ArrayList<>();
        items.addAll(this.cartItems.values());
        return items;
    }

    private void setItem(Integer id, CartItem cartItem){
        if(cartItem != null) {
            cartItems.put(id, cartItem);
            logger.debug("id = " + id + " " + cartItem);
        }
        else {
            logger.debug("Ignored empty cartItem");
        }
    }

    private void deleteItem(Integer id, CartItem cartItem){
        if(cartItem != null){
            cartItems.remove(id);
            logger.debug("id = " + id + " " + cartItem);
        }
        else {
            logger.debug("Ignored empty cartItem");
        }
    }

    private void updateTotalValues(){
        this.count = 0;
        this.sum = 0;
        for(CartItem item: cartItems.values()){
            this.count += item.getCount();
            this.sum += item.getSum();
        }
    }

}
