package com.lea.books.model.cart;

import com.lea.books.entity.catalog.Book;

public class CartItem {
    private int id;
    private Book book;
    private int count;
    private double price;
    private double sum;

    public void setId(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public int getCount() {
        return count;
    }

    public double getPrice() {
        return price;
    }

    public double getSum() {
        return sum;
    }

    public void updateQuantity(int count) {
        if(count>1000) count = 1000;

        this.count = count;
        this.sum = this.count*this.price;
    }

    public void updatePrice(double price) {
        this.price = price;
        this.sum = this.count*this.price;
    }

    @Override
    public String toString() {
        return "CartItem{" +
                "book=" + book +
                ", count=" + count+
                ", price=" + price +
                ", sum=" + sum +
                '}';
    }
}
