package com.lea.books.controller.home;

import com.lea.books.service.ui.BreadcrumbsBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.HandlerMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class PageController {
    @Autowired @Qualifier("pageBreadcrumbsBuilderConf")
    BreadcrumbsBuilder breadcrumbBuilder;

    @RequestMapping(value = {"/delivery","/help","/aboutus","/contacts",
            "/payment","/return","/loyalty","/partnership"},method = RequestMethod.GET)
    public String showDeliveryPage(Model model,HttpServletRequest request){
        String restOfTheUrl = (String) request.getAttribute(
            HandlerMapping.PATH_WITHIN_HANDLER_MAPPING_ATTRIBUTE);

        model.addAttribute("sectionPath", breadcrumbBuilder.build(restOfTheUrl));
        return "view.service";
    }
}
