package com.lea.books.controller.system;

import com.lea.books.entity.order.Order;
import com.lea.books.entity.user.User;
import com.lea.books.service.order.OrderService;
import com.lea.books.service.user.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ProfileController{
    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    private static final Logger logger = Logger.getLogger(ProfileController.class);

    @RequestMapping(value = {"/profile","/profile/personal"}, method = RequestMethod.GET)
    public String showUserProfilePersonalPage(Model model){
        User сurUser = userService.getCurrentUser();
        model.addAttribute("user", сurUser);
        model.addAttribute("SELECT_ProfilePersonal", "select");
        return "view.profile.personal";
    }

    @RequestMapping(value = {"/profile/orders"}, method = RequestMethod.GET)
    public String showUserProfileOrdersPage(Model model){
        User сurUser = userService.getCurrentUser();
        List <Order> orderList = orderService.selectByUser(сurUser);

        model.addAttribute("orderList", orderList);
        model.addAttribute("SELECT_ProfileOrders", "select");

        if(logger.isDebugEnabled()) {
            logger.debug("Select order list: " + orderList);
        }

        return "view.profile.orders.user";
    }

    @RequestMapping(value = {"/admin/orders"}, method = RequestMethod.GET)
    public String showAdminProfileOrdersPage(Model model){
        List <Order> orderList = orderService.selectAll();

        model.addAttribute("orderList", orderList);
        model.addAttribute("SELECT_ProfileAdminOrders", "select");

        if(logger.isDebugEnabled()) {
            logger.debug("Select order list: " + orderList);
        }

        return "view.profile.orders.admin";
    }
}
