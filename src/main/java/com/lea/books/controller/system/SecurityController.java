package com.lea.books.controller.system;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SecurityController{

    private static final Logger logger = Logger.getLogger(SecurityController.class);

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(@RequestHeader("referer") String referer){
        return "redirect:" + referer;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            logger.warn("Invalid username and password!");
            model.addObject("error", "Invalid username and password!");
        }

        model.setViewName("view.login");

        return model;
    }

    @RequestMapping(value = "/account/resend_password", method = RequestMethod.GET)
    public String resendPassword(@RequestHeader("referer") String referer){
        return "redirect:" + referer;
    }
}
