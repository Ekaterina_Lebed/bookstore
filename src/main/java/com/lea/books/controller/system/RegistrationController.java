package com.lea.books.controller.system;

import com.lea.books.entity.user.UserRole;
import com.lea.books.enums.RoleEnum;
import com.lea.books.entity.user.User;
import com.lea.books.service.system.SecurityService;
import com.lea.books.service.user.UserService;
import com.lea.books.service.system.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
public class RegistrationController{
    @Autowired
    RequestService requestService;

    @Autowired
    UserService userService;

    @Autowired
    SecurityService securityService;

    @RequestMapping(value = {"/user/registration"}, method = RequestMethod.GET)
    public String userShowRegistrationPage(){
        return "view.registration.dialog";
    }

    @RequestMapping(value = {"/user/registration/accept"}, method = RequestMethod.POST)
    public String userRegistrationAccept(HttpServletRequest request){
        User user = new User();
        user.setName(request.getParameter("user_name"));
        user.setLogin(request.getParameter("user_email"));
        user.setEmail(request.getParameter("user_email"));
        user.setPassword(request.getParameter("user_pass"));

        UserRole userRole = new UserRole();
        userRole.setUser(user);
        userRole.setRole(RoleEnum.ROLE_USER);
        user.getRoles().add(userRole);
        int status = userService.insert(user);

        if(status != 0){
            return "error.simple";
        }

        securityService.doAutoLogin(user);
        return "view.registration.accept";
    }

}
