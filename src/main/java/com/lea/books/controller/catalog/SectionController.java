package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Section;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.catalog.SectionService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SectionController{
    @Autowired
    SectionService sectionService;

    @Autowired
    BookService bookService;

    @Autowired
    ResourceUtils resourceUtils;

    @Autowired
    BreadcrumbsBuilder breadcrumbsBuilder;

    private static final Logger logger = Logger.getLogger(SectionController.class);
    private static final String VIEW_NAME_LIST = "view.bookList";

    @RequestMapping(value = {"/section"}, method = RequestMethod.GET)
    public String showBookList(@RequestParam("id") String _id, Model model) {
        int id = 0;

        try {
            id = Integer.parseInt(_id);
        } catch (Exception e) {
            logger.warn("ID (" + _id + ") is not a numerical value",e);
        }

        if(id<=0) {
            logger.warn("Ignored empty ID");
            return VIEW_NAME_LIST;
        }

        logger.debug("ID = " + id);

        Section section = sectionService.selectSectionById(id);
        List<Book>bookList = bookService.selectBySection(section);

        model.addAttribute("section", section);
        model.addAttribute("bookList", bookList);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build(section));
        resourceUtils.makeResources(bookService.getResources(bookList));

        return VIEW_NAME_LIST;
    }
}
