package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.apache.log4j.Logger;

@Controller
@RequestMapping(value = {"/book"})
public class BookController{
    @Autowired
    BookService bookService;

    @Autowired
    BreadcrumbsBuilder breadcrumbsBuilder;

    @Autowired
    ResourceUtils resourceUtils;

    private static final Logger logger = Logger.getLogger(BookController.class);

    @RequestMapping(method = RequestMethod.GET)
    public String showBookPage(@RequestParam("id") String _id, Model model) {
        int id = 0;

        try {
            id = Integer.parseInt(_id);
        } catch (NumberFormatException e) {
            logger.warn("ID (" + _id + ") is not a numerical value",e);
        }

        if(id<=0){
            logger.warn("Ignored empty ID");
            return "error.catalog";
        }

        logger.debug("ID = " + id);

        Book book = bookService.selectById(id);
        if(book==null){
            logger.warn("ID (" + _id + ") book is not found");
            return "error.catalog";
        }


        model.addAttribute("book", book);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build(book));
        resourceUtils.makeResources(bookService.getResources(book));

        return "view.book";
    }

}
