package com.lea.books.controller.catalog;

import com.lea.books.model.catalog.CatalogBranch;
import com.lea.books.service.catalog.*;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class CatalogController{
    @Autowired
    CatalogService catalogService;

    @Autowired
    BreadcrumbsBuilder breadcrumbsBuilder;

    private static final Logger logger = Logger.getLogger(CatalogController.class);

    @RequestMapping(value = {"/catalog"}, method = RequestMethod.GET)
    public String showCatalog(Model model){
        List<CatalogBranch> branches = catalogService.buildBranches(3);
        logger.debug("Catalog branches count:" + branches.size());

        model.addAttribute("catalogTree", branches);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build());

        return "view.catalog";
    }
}
