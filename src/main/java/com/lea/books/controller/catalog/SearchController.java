package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/search")
public class SearchController {
    @Autowired
    BookService bookService;

    @Autowired
    ResourceUtils resourceUtils;

    @Autowired
    BreadcrumbsBuilder breadcrumbsBuilder;

    private static final String VIEW_NAME_LIST = "view.bookList";

    @RequestMapping(method = RequestMethod.GET)
    public String search(@RequestParam("param") String param, Model model){
        List<Book> bookList = bookService.search(param);
        model.addAttribute("section", null);
        model.addAttribute("bookList", bookList);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build());
        resourceUtils.makeResources(bookService.getResources(bookList));

        return VIEW_NAME_LIST;
    }
}
