package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.model.catalog.BookSet;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.catalog.CategoryService;

import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CategoryController{
    @Autowired
    CategoryService categoryService;

    @Autowired
    BookService bookService;

    @Autowired
    ResourceUtils resourceUtils;

    @Autowired
    BreadcrumbsBuilder breadcrumbsBuilder;

    private static final Logger logger = Logger.getLogger(CategoryController.class);
    private static final String VIEW_NAME_LIST = "view.bookList";
    private static final String VIEW_NAME_SETS = "view.bookSets";

    @RequestMapping(value = {"/category"}, method = RequestMethod.GET)
    public String showBookList(@RequestParam("id") String _id, Model model) {
        int id = 0;

        try {
            id = Integer.parseInt(_id);
        } catch (Exception e) {
            logger.warn("ID (" + _id + ") is not a numerical value",e);
        }

        if(id <= 0) {
            logger.warn("Ignored empty ID");
            return VIEW_NAME_LIST;
        }

        logger.debug("ID = " + id);

        Category category = categoryService.selectById(id);
        List<Book>bookList = bookService.selectByCategory(category);

        model.addAttribute("section", category);
        model.addAttribute("bookList", bookList);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build(category));
        resourceUtils.makeResources(bookService.getResources(bookList));

        return VIEW_NAME_LIST;
    }

    @RequestMapping(value = {"/topsales"}, method = RequestMethod.GET)
    public String showBookListCategoryTopSales(Model model) {
        Category category = categoryService.topSalesCategory();

        if(category == null){
            model.addAttribute("sectionPath", breadcrumbsBuilder.build());
            logger.warn("Category is empty");
            return VIEW_NAME_LIST;
        }

        logger.debug("ID = " + category.getId());
        List<Book>bookList = bookService.selectByCategory(category);

        model.addAttribute("section",       category);
        model.addAttribute("bookList",      bookList);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build(category));
        resourceUtils.makeResources(bookService.getResources(bookList));

        return VIEW_NAME_LIST;
    }

    @RequestMapping(value = {"/novelty"}, method = RequestMethod.GET)
    public String showBookListCategoryNovelty(Model model) {
        Category category = categoryService.noveltyCategory();

        if(category == null) {
            model.addAttribute("sectionPath", breadcrumbsBuilder.build());
            logger.warn("Category is empty");
            return VIEW_NAME_LIST;
        }

        logger.debug("ID = " + category.getId());
        List<Book>bookList = bookService.selectByCategory(category);

        model.addAttribute("section",       category);
        model.addAttribute("bookList",      bookList);
        model.addAttribute("sectionPath", breadcrumbsBuilder.build(category));
        resourceUtils.makeResources(bookService.getResources(bookList));

        return VIEW_NAME_LIST;
    }

    @RequestMapping(value = {"/recommended"}, method = RequestMethod.GET)
    public String showBookListCategoryRecommended(Model model){
        Category noveltyCategory = categoryService.noveltyCategory();
        Category topSalesCategory = categoryService.topSalesCategory();

        if(noveltyCategory == null && topSalesCategory == null){
            logger.warn("Category \"novelty\" is empty");
            logger.warn("Category \"topSales\" is empty");
            return VIEW_NAME_SETS;
        }

        List<Category> categories = new ArrayList<>();
        if(noveltyCategory != null){
            logger.debug("ID = {" + noveltyCategory.getId());
            categories.add(noveltyCategory);
        }
        else {
            logger.warn("Category \"novelty\" is empty");
        }

        if(topSalesCategory != null){
            categories.add(topSalesCategory);
            logger.debug("ID = {" + topSalesCategory.getId());
        }
        else {
            logger.warn("Category \"topSales\" is empty");
        }

        List<BookSet> bookSets = bookService.buildBookSets(categories,10);
        model.addAttribute("bookSets", bookSets);
        resourceUtils.makeResources(bookService.getResourcesList(bookSets));

        return VIEW_NAME_SETS;
    }
}
