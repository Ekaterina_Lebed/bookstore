package com.lea.books.controller.cart;

import com.lea.books.entity.catalog.Book;
import com.lea.books.model.cart.Cart;
import com.lea.books.service.cart.CartService;
import com.lea.books.service.catalog.BookService;

import com.lea.books.service.system.RequestService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


@Controller
public class CartController {
    @Autowired
    BookService bookService;

    @Autowired
    CartService cartService;

    @Autowired
    RequestService requestService;

    private static final Logger logger = Logger.getLogger(CartController.class);

    @ResponseBody
    @RequestMapping(value = {"/cart/add","/cart/qw/add"}, method = RequestMethod.GET)
    public String addCartItem(@RequestParam("id") String _id, HttpServletRequest request){
        int id = parseID(_id);
        Cart cart = requestService.getCart(request);

        if(id>0) {
            Book book = bookService.selectById(id);
            cartService.addCartItem(cart, book, 1);
            logger.debug("Cart update: id="+id);
        }

        return cart.toJson();
    }

    @ResponseBody
    @RequestMapping(value = {"/cart/update"}, method = RequestMethod.GET)
    public String updateCartItem(@RequestParam("id") String _id, @RequestParam("count") String _count, HttpServletRequest request){
        int id = parseID(_id);
        int count = parseCount(_count);
        Cart cart = requestService.getCart(request);

        if(id > 0){
            Book book = bookService.selectById(id);
            cartService.updateCartItem(cart, book, count);
            logger.debug("Cart update: id="+id+", count="+count);
        }

        return cart.toJson();
    }

    @ResponseBody
    @RequestMapping(value = {"/cart/delete"}, method = RequestMethod.GET)
    public String deleteCartItem(@RequestParam("id") String _id, HttpServletRequest request){
        int id = parseID(_id);
        Cart cart = requestService.getCart(request);

        if(id > 0){
            cartService.deleteCartItem(cart, id);
            logger.debug("Cart update: id="+id);
        }

        return cart.toJson();
    }

    @ResponseBody
    @RequestMapping(value = {"/cart"}, method = RequestMethod.GET)
    public String getCart(HttpServletRequest request){
        Cart cart = requestService.getCart(request);
        return cart.toJson();
    }

    private int parseID(String _id){
        int id=0;
        try {
            id = Integer.parseInt(_id);
        } catch (Exception e) {
            logger.error("ID (" + _id + ") is not a numerical value",e);
        }

        if(logger.isDebugEnabled()){
            logger.debug("Request param: id = " + id);
        }
        return id;
    }

    private int parseCount(String _count){
        int count = 0;
        try {
            count = Integer.parseInt(_count);
        } catch (Exception e) {
            logger.error("Count (" + _count + ") is not a numerical value",e);
        }
        return count;
    }
}
