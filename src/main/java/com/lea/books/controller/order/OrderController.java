package com.lea.books.controller.order;

import com.lea.books.enums.DeliveryMethod;
import com.lea.books.enums.PaymentMethod;
import com.lea.books.entity.order.Order;
import com.lea.books.model.cart.Cart;
import com.lea.books.model.common.Contact;
import com.lea.books.entity.user.User;
import com.lea.books.service.cart.CartService;
import com.lea.books.service.order.OrderService;
import com.lea.books.service.system.RequestParams;
import com.lea.books.service.user.UserService;
import com.lea.books.service.system.RequestService;
import com.lea.books.utils.ResourceUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class OrderController{
    @Autowired
    OrderService orderService;

    @Autowired
    CartService cartService;

    @Autowired
    UserService userService;

    @Autowired
    RequestService requestService;

    @Autowired
    ResourceUtils resourceUtils;

    private static final Logger logger = Logger.getLogger(OrderController.class);

    @RequestMapping(value = {"/order/create"}, method = RequestMethod.GET)
    public String showOrderCreatePage(Model model, HttpServletRequest request){
        User user = userService.getCurrentUser();

        Cart cart = requestService.getCart(request);

        if(user != null) {
            logger.debug("Create order for user: " + user);

            model.addAttribute("userName",     user.getName());
            model.addAttribute("userLastName", user.getLastName());
            model.addAttribute("userContact",  userService.getContact(user));
            model.addAttribute("items",        cart.getItems());
            model.addAttribute("totalSum",     cart.getSum());
        }
        else {
            logger.warn("Current user is empty");
        }

        resourceUtils.makeResources(cart.getResources());
        return "view.order.create";
    }

    @RequestMapping(value = {"/order/accept"}, method = RequestMethod.POST)
    public String showOrderAcceptPage(Model model, HttpServletRequest request){
        RequestParams params = new RequestParams(request);

        Cart cart = requestService.getCart(request);
        Contact contact = params.getContact();

        if(cart.isEmpty()){
            logger.error("Order not accepted");
            return "error.simple";
        }

        User user = userService.getCurrentUser();
        user.setName(request.getParameter("orderUserName"));
        user.setLastName(request.getParameter("orderUserLastName"));
        userService.updateContact(user, contact);
        userService.insert(user);

        Order order = orderService.createNew();
        orderService.updateContact(order, contact);
        orderService.updateItems(order, cart.getItems());
        order.setDelivery(params.getDelivery("orderDelivery"));
        order.setPayment(params.getPayment("orderPayment"));
        order.setInfo(params.getString("orderInfo"));
        order.setUser(user);

        int status = orderService.insert(order);

        if(status == 0){
            cart.clear();
            logger.debug("Order accepted: \n" + order);
        }
        else {
            logger.error("Order not accepted: \n" + order);
            return "error.simple";
        }

        model.addAttribute("order", order);
        resourceUtils.makeResources(orderService.getResources(order));

        return "view.order.accept";
    }

    @RequestMapping(value = {"/order","/order/edit"}, method = RequestMethod.GET)
    public String showOrderViewPage(@RequestParam("id") String _id, Model model){
        int id = 0;

        try {
            id = Integer.parseInt(_id);
        } catch (Exception e) {
            logger.error("ID (" + _id + ") is not a numerical value",e);
        }

        if(id>0){
            logger.debug("request param: ID = " + id);
            model.addAttribute("order", orderService.selectById(id));
        }
        else {
            logger.warn("Ignored empty ID");
        }

        return "view.order.open";
    }

    @ModelAttribute
    public void populateModelClass(Model model) {
        model.addAttribute("deliveryList", DeliveryMethod.values());
        model.addAttribute("paymentList", PaymentMethod.values());
    }
}
