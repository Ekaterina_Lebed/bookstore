package com.lea.books.controller.order;

import com.lea.books.entity.order.Order;
import com.lea.books.entity.user.User;
import com.lea.books.service.order.OrderService;
import com.lea.books.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping(value = {"/order"})
public class OrderControllerRest {
    @Autowired
    OrderService orderService;

    @Autowired
    UserService userService;

    @ResponseBody
    @RequestMapping(value = {"/admin/select/all"}, method = RequestMethod.GET)
    List<Order> getOrderListByAdmin(){
        return orderService.selectAll();
    }

    @ResponseBody
    @RequestMapping(value = {"/user/select/all"}, method = RequestMethod.GET)
    List<Order> getOrderListByUser(){
        User сurUser = userService.getCurrentUser();
        return orderService.selectByUser(сurUser);
    }

}
