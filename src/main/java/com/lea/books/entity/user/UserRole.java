package com.lea.books.entity.user;

import com.lea.books.enums.RoleEnum;

import javax.persistence.*;

@Entity
@Table(name="UserRoles")
public class UserRole {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @ManyToOne
    @JoinColumn(name="userId")
    private User user;

    @Column(name="role")
    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "id=" + id +
                ", user=" + user +
                ", role=" + role +
                '}';
    }
}
