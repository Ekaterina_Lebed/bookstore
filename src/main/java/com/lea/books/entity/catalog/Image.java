package com.lea.books.entity.catalog;


import com.lea.books.utils.ResourceObj;

import javax.persistence.*;

@Entity
@Table(name="Images")
@SuppressWarnings("unused")
public class Image implements java.io.Serializable, ResourceObj {
    @Id
    @GeneratedValue
    @Column(name="id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="bFile")
    private byte[] bFile;

    @Transient
    private String link;

    public Image() {
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getBFile() {
        return this.bFile;
    }

    public void setBFile(byte[] bFile) {
        this.bFile = bFile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}

