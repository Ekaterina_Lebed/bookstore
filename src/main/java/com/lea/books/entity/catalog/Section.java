package com.lea.books.entity.catalog;

import javax.persistence.*;

@Entity
@Table(name="Section")
public class Section{
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @ManyToOne(cascade={CascadeType.ALL}/*,fetch = FetchType.LAZY*/)
    @JoinColumn(name="parent_id", nullable = true)
    private Section root;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Section getRoot() {
        return root;
    }

    public void setRoot(Section root) {
        this.root=root;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)return false;
        if (obj == this)return true;
        if (getClass() != obj.getClass())return false;

        Section pObj = (Section) obj;
        return (this.getId() == pObj.getId());
    }

    @Override
    public int hashCode(){
        final int PRIME = 31;
        int result = 1;
        result = PRIME * result + getId();
        return result;
    }
}
