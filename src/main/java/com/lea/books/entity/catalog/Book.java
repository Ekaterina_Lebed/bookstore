package com.lea.books.entity.catalog;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Book")
@SuppressWarnings("unused")
public class Book {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @Column(name="name")
    private String name;

    @Column(name="article")
    private String article;

    @ManyToOne
    @JoinColumn(name="section", nullable = true)
    private Section section;

    @Column(name="yearPublication")
    private int yearPublication;

    @Column(name="numberPublication")
    private int numberPublication;

    @Column(name="isbn")
    private String isbn;

    @Column(name="countPages")
    private int countPages;

    @Column(name="formatW")
    private int formatW;

    @Column(name="formatH")
    private int formatH;

    @Column(name="description")
    private String description;

    @Column(name="price")
    private double price;

    @Column(name="remains")
    private int remains;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name="BookCategories",
    joinColumns={@JoinColumn(name="idBook")},
    inverseJoinColumns={@JoinColumn(name="idCategory")})
    private List<Category> categories=new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    @JoinTable(name="BookAuthors",
    joinColumns={@JoinColumn(name="idBook",referencedColumnName="id")},
    inverseJoinColumns={@JoinColumn(name="idAuthor",referencedColumnName="id")})
    private List <Author> authors=new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="imageSrc", referencedColumnName = "name",insertable = false, updatable = false)
    private Image image;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors.clear();
        this.authors.addAll(authors);
    }

    public void setAuthor(Author author){
        this.authors.clear();
        this.authors.add(author);
    }

    public Section getSection() {
        return section;
    }

    public void setSection(Section section) {
        this.section = section;
    }

    public int getYearPublication() {
        return yearPublication;
    }

    public void setYearPublication(int yearOfPublication) {
        this.yearPublication = yearOfPublication;
    }

    public int getNumberPublication() {
        return numberPublication;
    }

    public void setNumberPublication(int numberPublication) {
        this.numberPublication = numberPublication;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getCountPages() {
        return countPages;
    }

    public void setCountPages(int countPages) {
        this.countPages = countPages;
    }

    public int getFormatW() {
        return formatW;
    }

    public void setFormatW(int formatW) {
        this.formatW = formatW;
    }

    public int getFormatH() {
        return formatH;
    }

    public void setFormatH(int formatH) {
        this.formatH = formatH;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public void setCategory(Category category){
        this.categories.add(category);
    }


    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getRemains() {
        return remains;
    }

    public void setRemains(int remains) {
        this.remains = remains;
    }

    public String getImageSrc() {
        return (image != null)?image.getLink():"";
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getAuthorsName(){
        String result = "";

        if(authors!=null) {
            for (Author author: authors) {
                result += (result.isEmpty() ? "" : ", ") + author.getName();
            }
        }

        return result;
    }
}

