package com.lea.books.entity.order;

import com.lea.books.enums.*;
import com.lea.books.entity.user.User;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@Table(name="DocOrder")
public class Order {
    @Id
    @GeneratedValue
    @Column(name="id")
    private int id;

    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name="docNumber", unique = true)
    private String number;

    @Column(name="docDate", columnDefinition="DATETIME")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name="phone")
    private String phone;

    @Column(name="email")
    private String email;

    @Column(name="city")
    private String city;

    @Column(name="street")
    private String street;

    @Column(name="house")
    private String house;

    @Column(name="flat")
    private String flat;

    @Column(name="statusCode")
    @Enumerated(EnumType.ORDINAL)
    private StatusOrder status;

    @Column(name="deliveryCode")
    @Enumerated(EnumType.ORDINAL)
    private DeliveryMethod delivery;

    @Column(name="paymentCode")
    @Enumerated(EnumType.ORDINAL)
    private PaymentMethod payment;

    @Column(name="info")
    private String info;
    @Column(name="sum")
    private double sum;

    @ManyToOne
    @JoinColumn(name="user")
    private User user;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL,mappedBy = "order")
    @Fetch(FetchMode.SUBSELECT)
    private List<OrderItem> items=new ArrayList<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public double getSum() {
        return sum;
    }

    public void setSum(double sum) {
        this.sum= sum;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setDelivery(DeliveryMethod delivery) {
        this.delivery = delivery;
    }

    public DeliveryMethod getDelivery(){
        return delivery;
    }

    public void setPayment(PaymentMethod payment) {
        this.payment = payment;
    }

    public PaymentMethod getPayment(){
        return payment;
    }

    public StatusOrder getStatus(){
        return status;
    }

    public void setStatus(StatusOrder status){
        this.status=status;
    }

    public String getName(){
        SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

        return "Заказ №"+getNumber()+" от "+DATE_FORMAT.format(getDate());
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", date=" + date +
                ", user=" + user +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house=" + house +
                ", flat=" + flat +
                ", info='" + info + '\'' +
                ", sum=" + sum +
                ", items=" + items +
                ", status=" + status +
                ", payment=" + payment +
                ", delivery=" + delivery +
                '}';
    }

}
