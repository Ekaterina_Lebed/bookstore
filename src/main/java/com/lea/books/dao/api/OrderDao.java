package com.lea.books.dao.api;

import com.lea.books.entity.order.Order;
import com.lea.books.entity.user.User;

import java.util.List;


public interface OrderDao {
    void insert(Order order);

    void update(Order order);

    void delete(Order order);

    Order selectById(int id);

    List<Order> selectByUser(User user);

    List<Order> selectAll();
}
