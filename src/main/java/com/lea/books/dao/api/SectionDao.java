package com.lea.books.dao.api;

import com.lea.books.entity.catalog.Section;

import java.util.List;

public interface SectionDao {
    void insert(Section item);

    void update(Section  item);

    void delete(Section  item);

    Section selectById(int id);

    List<Section> selectAll();

    List<Section> selectByParent(Section parent);

    List<Section> selectBySection(Section parent);
}
