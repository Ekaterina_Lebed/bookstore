package com.lea.books.dao.api;

import com.lea.books.entity.catalog.Category;

public interface CategoryDao {
    void insert(Category item);

    void update(Category item);

    Category selectById(int id);
}
