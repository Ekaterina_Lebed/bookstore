package com.lea.books.dao.api;

import com.lea.books.entity.user.User;

import java.util.List;

public interface UserDao {
    void insert(User item);

    void update(User item);

    void delete(User item);

    User selectByLogin(String name);

    List<User> selectAll();

}
