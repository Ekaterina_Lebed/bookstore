package com.lea.books.dao.api;

import com.lea.books.entity.catalog.Image;

import java.util.List;

public interface ImageDao {
    void insert(Image item);

    void update(Image item);

    void delete(Image item);

    Image selectById(int id);

    Image select(String name);

    List<Image> select(List<String> listOfNames);
}
