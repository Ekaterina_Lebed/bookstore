package com.lea.books.dao.api;

import com.lea.books.entity.catalog.Author;

import java.util.List;


public interface AuthorDao  {
    void insert(Author item);

    void update(Author item);

    List<Author> selectAll();

    Author selectById(int id);
}
