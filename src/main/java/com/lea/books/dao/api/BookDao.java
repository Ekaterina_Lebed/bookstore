package com.lea.books.dao.api;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.entity.catalog.Section;

import java.util.List;


public interface BookDao {
    void insert(Book item);

    void update(Book item);

    void deleteById(Book item);

    Book selectById(int id);

    List<Book> selectAll();

    List<Book> selectBySection(Section section);

    List<Book> selectBySections(List<Section> sections);

    List<Book> selectByCategory(Category category);

    List<Book> selectByCategory(Category category,int count);

    List<Book> selectByName(String name);
}
