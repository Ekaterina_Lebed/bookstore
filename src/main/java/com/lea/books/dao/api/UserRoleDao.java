package com.lea.books.dao.api;

import com.lea.books.entity.user.User;
import com.lea.books.entity.user.UserRole;

public interface UserRoleDao {
    void insert(UserRole item);

    void delete(UserRole item);

    void deleteByUser(User user);
}
