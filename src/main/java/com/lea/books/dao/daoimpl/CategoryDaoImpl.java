package com.lea.books.dao.daoimpl;

import com.lea.books.dao.api.CategoryDao;
import com.lea.books.entity.catalog.Category;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class CategoryDaoImpl implements CategoryDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void insert(Category item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Override
    @Transactional
    public void update(Category item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    @Transactional
    public Category selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Category)session.get(Category.class, id);
    }
}
