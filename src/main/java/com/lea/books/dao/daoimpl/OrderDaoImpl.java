package com.lea.books.dao.daoimpl;


import com.lea.books.dao.api.OrderDao;
import com.lea.books.entity.order.Order;
import com.lea.books.entity.user.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class OrderDaoImpl implements OrderDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void insert(Order item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Transactional
    @Override
    public void update(Order item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Transactional
    @Override
    public void delete(Order item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Transactional
    @Override
    public Order selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Order)session.get(Order.class, id);
    }

    @Transactional
    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectByUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Order.class)
            .add(Restrictions.eq("user", user));

        return criteria.list();
    }

    @Transactional
    @Override
    @SuppressWarnings("unchecked")
    public List<Order> selectAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Order.class).list();
    }
}
