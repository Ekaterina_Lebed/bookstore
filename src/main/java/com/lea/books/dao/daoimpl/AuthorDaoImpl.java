package com.lea.books.dao.daoimpl;
import com.lea.books.dao.api.AuthorDao;
import com.lea.books.entity.catalog.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AuthorDaoImpl implements AuthorDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void insert(Author item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Override
    @Transactional
    public void update(Author item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Author> selectAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Author.class).list();
    }

    @Override
    @Transactional
    public Author selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Author)session.get(Author.class, id);
    }
}
