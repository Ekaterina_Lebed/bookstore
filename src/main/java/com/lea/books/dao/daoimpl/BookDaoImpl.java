package com.lea.books.dao.daoimpl;

import com.lea.books.dao.api.BookDao;
import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.entity.catalog.Section;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BookDaoImpl implements BookDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void insert(Book item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Override
    @Transactional
    public void update(Book item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    @Transactional
    public void deleteById(Book item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Override
    @Transactional
    public Book selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Book)session.get(Book.class, id);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Book> selectAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Book.class).list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Book> selectByName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Book.class).add(Restrictions.eq("name", name));
        return criteria.list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Book> selectBySection(Section section) {
        if(section == null){
            return  null;
        }

        Session session = sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Book where section=?")
            .addEntity(Section.class);
        query.setInteger(0,section.getId());

        return query.list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Book> selectBySections(List<Section> sectionList) {
        if(sectionList == null || sectionList.isEmpty()){
            return new ArrayList<>();
        }

        List <Integer> sectionListId = toListId(sectionList);
        if(sectionListId.isEmpty()){
            return new ArrayList<>();
        }

        Session session=sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Book where section in(:param)")
            .addEntity(Book.class);
        query.setParameterList("param", sectionListId);

        return query.list();
    }

    @Override
    @Transactional
    public List<Book> selectByCategory(Category category) {
        return selectByCategory(category,0);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Book> selectByCategory(Category category, int countMax) {
        if(category == null){
            return new ArrayList<>();
        }

        Session session = sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Book where id in(select idBook from BookCategories where idCategory=?)")
            .addEntity(Book.class);
        if(countMax > 0) query.setMaxResults(countMax);

        query.setInteger(0,category.getId());

        return query.list();
    }

    private List <Integer> toListId(List<Section> list){
        List <Integer> listId = new ArrayList<>();

        for (Section section : list){
            if(section == null) continue;
            listId.add(section.getId());
        }
        return listId;
    }

}
