package com.lea.books.dao.daoimpl;

import com.lea.books.dao.api.UserDao;
import com.lea.books.entity.user.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.NoSuchElementException;

@Repository
public class UserDaoImpl implements UserDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void insert(User item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Transactional
    @Override
    public void update(User item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Transactional
    @Override
    public void delete(User item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Transactional
    @Override
    public User selectByLogin(String login) {
        Session session = sessionFactory.getCurrentSession();
        Object result = session.createCriteria(User.class)
            .add(Restrictions.eq("login",login)).uniqueResult();

        if (result == null) {
            throw new NoSuchElementException("User by login: " + login);
        }

        return (User)result;
    }

    @Transactional
    @Override
    @SuppressWarnings("unchecked")
    public List<User> selectAll() {
        Session session = sessionFactory.getCurrentSession();
        return (List<User>)session.createCriteria(User.class).list();
    }
}
