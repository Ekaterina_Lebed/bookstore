package com.lea.books.dao.daoimpl;


import com.lea.books.dao.api.UserRoleDao;
import com.lea.books.entity.user.User;
import com.lea.books.entity.user.UserRole;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public class UserRoleImpl implements UserRoleDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void insert(UserRole item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Transactional
    @Override
    public void delete(UserRole item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Transactional
    @Override
    public void deleteByUser(User user) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "delete from UserRole where user= :user";
        session.createQuery(hql).setEntity("user", user).executeUpdate();
    }
}
