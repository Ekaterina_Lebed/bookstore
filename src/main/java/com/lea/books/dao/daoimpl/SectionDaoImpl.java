package com.lea.books.dao.daoimpl;

import com.lea.books.dao.api.SectionDao;
import com.lea.books.entity.catalog.Section;
import org.hibernate.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SectionDaoImpl implements SectionDao {
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    @Transactional
    public void insert(Section item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Override
    @Transactional
    public void update(Section item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    @Transactional
    public void delete(Section  item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Override
    @Transactional
    public Section selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Section)session.get(Section.class, id);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Section> selectAll() {
        Session session = sessionFactory.getCurrentSession();
        return session.createCriteria(Section.class).list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Section> selectByParent(Section parent) {

        Session session = sessionFactory.getCurrentSession();

        if(parent == null) {
            Query query = session
                .createSQLQuery("select * from Section where parent_id is null")
                .addEntity(Section.class);
            return query.list();
        }

        Query query = session
            .createSQLQuery("select * from Section where parent_id=?")
            .addEntity(Section.class);
        query.setInteger(0, parent.getId());

        return query.list();
    }

    @Transactional
    @SuppressWarnings("unchecked")
    private List<Section> selectByParent(List<Section> parentList) {
        if(parentList==null || parentList.isEmpty()){
            return null;
        }

        List <Integer> listId = toListId(parentList);
        if(listId.isEmpty()){
            return null;
        }

        Session session = sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Section where parent_id in(:listId)")
            .addEntity(Section.class);
        query.setParameterList("listId", listId);

        return query.list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Section> selectBySection(Section section) {
        if(section == null){
            return new ArrayList<>();
        }

        Session session = sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Section where parent_id=?")
            .addEntity(Section.class);
        query.setInteger(0, section.getId());
        List<Section> list = query.list();

        List<Section> subList = selectByParent(list);
        if(subList!=null){
            list.addAll(subList);
        }

        list.add(section);
        return list;
    }

    private List <Integer> toListId(List<Section> list){
        List <Integer> listId = new ArrayList<>();
        for (Section section : list){
            if(section == null)continue;
            listId.add(section.getId());
        }
        return listId;
    }
}
