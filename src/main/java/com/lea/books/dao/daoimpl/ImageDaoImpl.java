package com.lea.books.dao.daoimpl;

import com.lea.books.dao.api.ImageDao;
import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Image;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ImageDaoImpl implements ImageDao{
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insert(Image item) {
        Session session = sessionFactory.getCurrentSession();
        session.saveOrUpdate(item);
    }

    @Override
    public void update(Image item) {
        Session session = sessionFactory.getCurrentSession();
        session.update(item);
    }

    @Override
    public void delete(Image item) {
        Session session = sessionFactory.getCurrentSession();
        session.delete(item);
    }

    @Override
    public Image selectById(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (Image)session.get(Image.class, id);
    }

    @Override
    public Image select(String name) {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Image.class).add(Restrictions.eq("name", name));
        criteria.setMaxResults(1);
        return (Image) criteria.uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Image> select(List<String> listOfNames){
        Session session = sessionFactory.getCurrentSession();
        Query query = session
            .createSQLQuery("select * from Image where section in(:listOfNames)")
            .addEntity(Book.class);
        query.setParameterList("listOfNames", listOfNames);

        return query.list();
    }

}
