var UserValidator = (function() {
    function showError(formElement,isValid,errMessage) {
        var childNodes = formElement.parentNode.childNodes;

        for(var i = 0; i < childNodes.length; i++){
            var elementNode = childNodes.item(i);

            if(elementNode.className == "error-info"){
                elementNode.innerHTML = (isValid) ? "" : errMessage;
            }
        }
    }

    var module = {};

    module.validate = function (){
        var elementField;
        var isValid;
        var result = true;

        elementField = document.getElementById("userName");
        isValid = ValueValidator.isUserLoginValid(elementField.value);
        showError(elementField, isValid, "Не верно заполнено имя");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("userEmail");
        isValid = ValueValidator.isEmailValid(elementField.value);
        showError(elementField, isValid, "Не верно заполнен e-mail");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("userPass");
        var elementField2 = document.getElementById("userPass2");

        if(elementField.value == elementField2.value){
            isValid = ValueValidator.isPasswordValid(elementField.value);
            showError(elementField, isValid, "Не верно заполнен пароль");
            showError(elementField2, isValid, "Не верно заполнен пароль");
            result = (!isValid) ? isValid : result;
        }
        else{
            showError(elementField, false, "Не совпадают значения паролей");
            showError(elementField2, false, "Не совпадают значения паролей");
            result = false;
        }

        console.log("result = " + result);
        return result;
    };

    return module;

})();
