
var eventHandler = (function() {

    var module = {};

    module.goToBottomPage=function(){
        window.scrollTo(0, 0);
    };

    module.goToReferrerUrl=function(){
        window.location.href = document.referrer;
    };

    return module;
})();


