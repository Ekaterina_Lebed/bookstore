
var cartEventHandler = (function() {

    function cartRequestModule(){
        var module = {};

        function selectCartItem(cartState,id){
            if(id == null)return null;
            for (var index in cartState.cartItems){
                var cartItem = cartState.cartItems[index];
                if(String(cartItem.id) == String(id)){
                    return cartItem;
                }
            }
            return null;
        }

        function setVersion(path){
            var now = new Date();
            var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours());
            var diff = now - today;

            var result = path.substr(0);

            if(result.indexOf('?') > 0){
                result = result + '&ver=' + diff
            }
            else {
                result = result + '?ver=' + diff
            }

            return result;
        }

        function doRequestQuickBuy(path){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', Config.other()['serverHost'] + setVersion(path));
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) {
                    return;
                }

                var cartState = JSON.parse(xhr.responseText);
                cartView.updateHeaderView(cartState);
            }
        }

        function doRequestGET(path,idGood){
            var xhr = new XMLHttpRequest();
            xhr.open('GET', Config.other()['serverHost'] + setVersion(path));
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) {
                    return;
                }

                var cartState = JSON.parse(xhr.responseText);
                var cartItem  = selectCartItem(cartState, idGood);

                cartView.updateHeaderView(cartState);
                cartView.updateItemsExistingView(cartState);
                cartView.updateSelectedItemView(cartItem);
            }
        }

        module.doQuickBuy = function (idGood){
            var path = '/cart/qw/add?id=' + idGood;
            doRequestQuickBuy(path);
        };

        module.doAddItem = function (idGood){
            var path = '/cart/add?id=' + idGood;
            doRequestGET(path);
        };

        module.doUpdateCount = function (idGood,count){
            if(isNaN(count) || count < 1) count = 1;
            else if(count > 100) count = 100;

            var path = '/cart/update?id=' + idGood + '&count=' + count;
            doRequestGET(path,idGood);
        };

        module.doDeleteItem = function(idGood){
            var path = '/cart/delete?id=' + idGood;
            doRequestGET(path);
        };

        module.doUpdateHeader = function(){
            var path = '/cart';
            doRequestGET(path);
        };

        return module;
    }

    function cartFormatModule(){
        var module = {};

        module.declOfNum=function declOfNum(number, titles){
            var cases = [2, 0, 1, 1, 1, 2];
            return titles[(number%100>4 && number%100<20)? 2 : cases[(number%10<5) ? number%10 : 5]];
        };

        module.declOfNumForGoodsSeconds=function declOfNumForGoodsSeconds(i){
            var titles = ['секунду', 'секунды', 'секунд'];
            return declOfNum (i, titles);
        };

        module.declOfNumForGoods=function declOfNumForGoods(i){
            var titles = ['товар', 'товара', 'товаров'];
            return this.declOfNum(i, titles);
        };

        return module;
    }

    function cartViewModule(){
        var module = {};

        function getCartItemsElement(){
            return document.getElementById("cart-items");
        }

        function getCartItemsElements(){
            return document.getElementsByClassName("cart-item");
        }

        function getCartStateSumElement(){
            return document.getElementById('totalSum');
        }

        function getCartItemsId(cartState){
            var result = [];
            var cartItem;
            for (var ind in cartState.cartItems){
                cartItem = cartState.cartItems[ind];
                result[result.length] = "cartItem[" + cartItem.id + "]";
            }
            return result;
        }


        function checkExistingOfCartItems(cartState, isExistParam){
            var ElementsSet = getCartItemsElements();
            var idSet = getCartItemsId(cartState);

            var result = [];
            for (var index = 0; index < ElementsSet.length; index++){
                var elementNode = ElementsSet[index];
                var isExist = false;

                for(var ind in idSet){
                    if(elementNode.id == idSet[ind]){
                        isExist=true;
                        break;
                    }
                }

                if(isExist == isExistParam){
                    result[result.length] = elementNode;
                }
            }
            return result;
        }

        function removeCartItems(itemsElements){
            var cartElement = getCartItemsElement();
            if(cartElement == null) return;

            for(var i=0; i< itemsElements.length; i++){
                cartElement.removeChild(itemsElements[i]);
            }
        }

        module.getItemCount = function(idGood){
            var elementCount = document.getElementById('itemCount[' + idGood + ']');
            if(elementCount != null) {
                return Number(elementCount.value);
            }
            return NaN;
        };

        module.updateSelectedItemView = function(cartItem){
            if(cartItem == null) return;

            var elementCount = document.getElementById('itemCount['+ cartItem.id + ']');
            var elementSum = document.getElementById('itemSum['+ cartItem.id + ']');

            if(elementCount != null) elementCount.value = cartItem.count.toFixed(0);
            if(elementSum != null) elementSum.innerHTML = cartItem.sum.toFixed(2);
        };

        module.updateItemsExistingView = function(cartState){
            var cartItemsElement = getCartItemsElement();
            if(cartItemsElement == null) return;

            var itemsToRemove = checkExistingOfCartItems(cartState, false);
            removeCartItems(itemsToRemove);

            var elementSum = getCartStateSumElement();
            if(elementSum != null) elementSum.innerHTML = cartState.sum.toFixed(2);
        };

        module.updateHeaderView = function(cartState){
            document.getElementById("cartGoods").innerHTML =
                cartState.count.toFixed(0) + " " +
                formatHandler.declOfNumForGoods(cartState.count);

            document.getElementById("cartSum").innerHTML =
                cartState.sum.toFixed(2) + " " +
                cartState.currency.name;
        };

        return module;
    }

    var cartRequest = cartRequestModule();
    var formatHandler = cartFormatModule();
    var cartView = cartViewModule();

    var module = {};

    module.quickBuy = function(idGood){
        cartRequest.doQuickBuy(idGood);
    };

    module.addItem = function(idGood){
        cartRequest.doAddItem(idGood);
    };

    module.deleteItem = function(idGood){
        cartRequest.doDeleteItem(idGood);
    };

    module.setCount = function(idGood){
        var count = cartView.getItemCount(idGood);
        cartRequest.doUpdateCount(idGood, count);
    };

    module.increaseCount = function(idGood){
        var count = cartView.getItemCount(idGood)+1;
        cartRequest.doUpdateCount(idGood, count);
    };

    module.reduceCount = function(idGood){
        var count = cartView.getItemCount(idGood) - 1;
        cartRequest.doUpdateCount(idGood, count);
    };

    module.updateHeader = function(){
        cartRequest.doUpdateHeader();
    };

    return module;

})();