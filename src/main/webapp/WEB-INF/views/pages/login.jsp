<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
    <title>Login Page</title>
    <style>
        .error {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            border-color: #ebccd1;
            color: #a94442;
            background-color: #f2dede;
        }

        .msg {
            padding: 15px;
            margin-bottom: 20px;
            border: 1px solid transparent;
            border-radius: 4px;
            color: #31708f;
            background-color: #d9edf7;
            border-color: #bce8f1;
        }

        #login-box {
            width: 300px;
            padding: 20px;
            margin: 100px auto;
            background: #fff;
            -webkit-border-radius: 2px;
            -moz-border-radius: 2px;
            border: 1px solid #000;
        }
    </style>
</head>

<body onload='document.loginForm.username.focus();'>

<div id="login-box" class="b-popup">
    <div class="b-popup-content">
        <h3>Login with Username and Password</h3>

        <c:if test="${not empty error}">
            <div class="error">${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
            <div class="msg">${msg}</div>
        </c:if>

        <form name='loginForm' action="login" method='POST'>
        <table>
            <tr>
                <td>
                    <label for="username">User:</label>
                </td>
                <td>
                    <input id="username" name='username' type='text' value=''>
                </td>
            </tr>
            <tr>
                <td>
                    <label for="password">Password:</label>
                </td>
                <td>
                    <input id='password' name='password' type='password'/>
                    <small><a href="<c:url value="/account/resend_password"/>">Forgot?</a></small>
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <input name="submit" type="submit" value="submit" />
                </td>
            </tr>
            <tr>
                <td colspan='2'>
                    <input type="checkbox" id="remember_me" name="remember_me"/>
                    <label for="remember_me" class="inline">Remember me</label>
                </td>
            </tr>
        </table>

        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    </form>
    </div>
</div>

</body>
</html>


