
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="books-section">
    <c:forEach var="itemSection" items="${sectionPath}">
        <a class="books-section-path" href="<c:url value="${itemSection.link}"/>">
            ${itemSection.name}
        </a>
        &rarr;
    </c:forEach>
</div>

