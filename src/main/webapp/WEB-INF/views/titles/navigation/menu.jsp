
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<table class="hmenu-main">
    <tbody>
    <tr>
        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=10000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-computer"></div>
                    <div class="navsite-text">Компьютерная литература</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=300000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-economy"></div>
                    <div class="navsite-text">Экономика</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=600000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-design"></div>
                    <div class="navsite-text">Дизайн</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=202"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-technique"></div>
                    <div class="navsite-text">Техническая литература</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=203"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-hobby"></div>
                    <div class="navsite-text">Хобби и увлечения</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=400000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-psy"></div>
                    <div class="navsite-text">Психология</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=200000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-low"></div>
                    <div class="navsite-text">Юридичская литература</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=500000"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-science"></div>
                    <div class="navsite-text">Научная литература</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=207"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-medicine"></div>
                    <div class="navsite-text">Красота и здоровье</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=208"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-imaginative"></div>
                    <div class="navsite-text">Художественная литература</div>
                </div>
            </a>
        </td>

        <td class="item-catalog">
            <a class="navsite-a" href="<c:url value="/section?id=209"/>">
                <div class="navsite">
                    <div class="navsite_icon icon-cognitive"></div>
                    <div class="navsite-text">Познавательная литература</div>
                </div>
            </a>
        </td>
    </tr>
    </tbody>
</table>



