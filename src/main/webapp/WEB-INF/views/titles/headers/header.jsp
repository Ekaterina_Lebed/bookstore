<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<div class="header-section">
    <div class="logo-area">
        <a href="<c:url value="/home"/>">
            <div class="logo-img"></div>
        </a>
    </div>

    <div class="search-area">
        <div class="search">
            <input type="text" name="q" value="Поиск..."
                class="search-field" id="insearch"
                style="width: 90%;background: #EFEFEF;"
                onfocus="if(this.value == 'Поиск...') { this.value = ''; }"
                onblur="if(this.value == '') { this.value = 'Поиск...'; }">
            <a class="search-button" href="<c:url value="/search?param="/>"></a>
        </div>

        <div class="contacts">
            <c:forEach var="contactItem" items="${contactPhones}">
                <div class="contacts-item">${contactItem}</div>
            </c:forEach>
        </div>
    </div>

    <div class="cart-area">
        <div class="asd">
            <div class="cart-icon">&nbsp;</div>
            <div class="cart-info">
                <div class="cart-info-goods" id="cartGoods" ></div>
                <div class="cart-info-sum" id="cartSum"></div>
                <a class="btn ibutton-orange ibutton-checkout" href="<c:url value="/order/create"/>">
                    Создать заказ
                </a>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        window.cartEventHandler.updateHeader();
    </script>
</div>
