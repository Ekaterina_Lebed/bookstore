
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="hmenu-content">
    <table class="itable">
        <tbody>

        <tr class="itable-item">
            <th>Номер</th>
            <th>Дата</th>
            <th>Пользователь</th>
            <th>Телефон</th>
            <th>Город</th>
            <th>Доставка</th>
            <th>Вид оплаты</th>
            <th>Сумма</th>
            <th>Статус</th>
        </tr>

        <c:forEach var="order" items="${orderList}">
            <tr class="itable-item">
                <td>
                    <a href="<c:url value="/order/edit?id=${order.id}"/>">
                        ${order.number}
                    </a>
                </td>
                <td>
                    <fmt:formatDate value="${order.date}" pattern="yyyy-MM-dd HH:mm" />
                </td>
                <td>${order.user.name} ${order.user.lastName}</td>
                <td>${order.phone}</td>
                <td>${order.city}</td>
                <td>${order.delivery.name}</td>
                <td>${order.payment.name}</td>
                <td class="sum">
                    <fmt:formatNumber type="number" minFractionDigits="2" value="${order.sum}"/>
                </td>
                <td>${order.status.name}</td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
</div>





