
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div class="content-header">
    Спасибо, Ваш заказ принят!
</div>

<div class="content-body">
    <div class="content-body-left2">
        <div class="order-header item-h0">
            Заказ № ${order.number}
        </div>

        <div class="list-items">
            <c:forEach var="cartItem" items="${order.items}">
                <div class="list-item">
                    <a href="<c:url value="/book?id=${cartItem.book.id}"/>">
                        <div class="list-item-icon">
                            <img class="book-img60" src="<c:url value="${cartItem.book.imageSrc}"/>" border="0" align="">
                        </div>
                    </a>

                    <div class="list-item-body">
                        <a href="<c:url value="/book?id=${cartItem.book.id}"/>">
                            <div class="title item-h1">
                                ${cartItem.book.name}
                            </div>
                        </a>

                        <div class="info item-h1">
                            <span class="count">
                                ${cartItem.count}<em>шт.</em>
                            </span>

                            <span class="sum">
                                Сумма:
                                <fmt:formatNumber type="number" minFractionDigits="2" value="${cartItem.sum}"/>
                                <em>UAH</em>
                            </span>
                        </div>
                    </div>
                </div>
            </c:forEach>

            <div class="total-item">
                <div class="list-item-icon">
                </div>

                <div class="list-item-body">
                    <span class="sum">
                        Итого:
                        <span class="item-sum-total">
                            <fmt:formatNumber type="number" minFractionDigits="2" value="${order.sum}"/>
                            <em>UAN</em>
                        </span>
                    </span>
                </div>
            </div>

        </div>
    </div>

    <div class="content-body-right2">
        <div class="ibutton-print">
            Распечатать заказ
        </div>
    </div>

</div>
