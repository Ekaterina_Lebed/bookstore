<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>


<div class="main-content">
    <div class="container" style="display: table">
        <div class="container-left" style="display: table-cell">
            <img class="image-view" src="<c:url value="${book.imageSrc}"/>" title="" alt="" border="0" align=""/>
            <div class="image-text">Артикул: <span class="highlight">${book.article}</span></div>
        </div>

        <div class="container-right" style="display: table-cell">
            <div class="container-header">
                <a class="header-h1" href="<c:url value="/book?id=${book.id}"/>">${book.name}</a>
                <div class="header-h2" >
                    ${book.authorsName}
                </div>
            </div>

            <div class="property-list">
                <div>Год издания: ${book.yearPublication}</div>
                <div>Номер издания: ${book.numberPublication}</div>
                <div>В наличии: ${book.remains}</div>
                <div>ISBN: ${book.isbn}</div>
                <div>Количество страниц: ${book.countPages}</div>
                <div>Формат: ${book.formatW} x ${book.formatH}</div>
            </div>

            <div class="action-section">
                <div class="price">
                    <fmt:formatNumber type="number" minFractionDigits="2" value="${book.price}"/>
                    <em>${currencyApp}</em>
                </div>

                <button type="submit" class="clear" onclick="cartEventHandler.quickBuy('${book.id}')">
                    <div class="btn ibutton-orange ibutton-buy">Купить</div>
                </button>
            </div>
        </div>
    </div>

    <div class="description">
        <div class="h1">Описание книги:</div>
        <div class="text1">${book.description}</div>
    </div>
</div>
