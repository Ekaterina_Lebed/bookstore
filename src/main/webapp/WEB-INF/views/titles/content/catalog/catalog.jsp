<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div class="catalog-section">
    <c:forEach var="catalogBranch" items="${catalogTree}">
        <div class="catalog-branch">
            <div class="branch-header">
                <a href="<c:url value="/section?id=${catalogBranch.section.id}"/>">
                    ${catalogBranch.section.name}
                </a>
            </div>

            <ul class="branch-level-h1">
                <c:forEach var="subBranch" items="${catalogBranch.subBranches}">
                    <li>
                        <a href="<c:url value="/section?id=${subBranch.section.id}"/>">
                            ${subBranch.section.name}
                        </a>
                    </li>

                    <ul class="branch-level-h2">
                        <c:forEach var="subBranch" items="${subBranch.subBranches}">
                            <li>
                                <a href="<c:url value="/section?id=${subBranch.section.id}"/>">
                                    ${subBranch.section.name}
                                </a>
                            </li>
                        </c:forEach>
                    </ul>
                </c:forEach>
            </ul>
        </div>
    </c:forEach>
</div>


