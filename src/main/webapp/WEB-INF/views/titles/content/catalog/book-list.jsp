<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<!--BOOK LIST-->
<div class="main-content__section">
    <c:forEach var="book" items="${bookList}">
        <div class="book-item">
            <div class="item-image">
                <a href="<c:url value="/book?id=${book.id}"/>">
                    <div class="item-image-view">
                        <img src="<c:url value="${book.imageSrc}"/>" alt="${book.name}">
                    </div>
                </a>
            </div>

            <div class="item-content">
                <a href="<c:url value="/book?id=${book.id}"/>">
                    <div class="title item-h1">${book.name}</div>
                </a>

                <span class="price">
                    <fmt:formatNumber type="number" minFractionDigits="2" value="${book.price}"/>
                    <em>${currencyApp}</em>
                </span>

                <button class="ibutton-quickbuy" type="submit" name="buyfromcatalog" onclick="cartEventHandler.quickBuy('${book.id}')">
                </button>
            </div>
        </div>
    </c:forEach>
</div>