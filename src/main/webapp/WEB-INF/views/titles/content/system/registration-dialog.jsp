<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<script type="text/javascript" src="<c:url value="/resources/script/validator/ValueValidator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/script/validator/UserValidator.js"/>"></script>

<div class="content-header">
    Создать учетную запись
</div>

<div class="content-body">
    <div class="content-body-left2">
        <form method="POST" action="<c:url value="/user/registration/accept"/>" onsubmit="return UserValidator.validate();">
            <table class="order-fields">
                <tbody>
                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userName">
                                Имя<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input id="userName" class="elem input-elem" name="user_name" type="text" value=""/>
                            <label for="userName" class="error-info"></label>
                        </td>
                    </tr>

                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userEmail">
                                Электронная почта<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input id="userEmail" class="elem input-elem" name="user_email" type="text" value=""/>
                            <label for="userEmail" class="error-info"></label>
                        </td>
                    </tr>

                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userPass">
                                Пароль<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input id="userPass" class="elem input-elem"  name="user_pass" type="password" value=""/>
                            <label for="userPass" class="error-info"></label>
                        </td>
                    </tr>

                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userPass2">
                                Повторите пароль<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input id="userPass2" class="elem input-elem" name="user_pass2" type="password" value=""/>
                            <label for="userPass2" class="error-info"></label>
                        </td>
                    </tr>

                    <tr class="ibutton-panel">
                        <td>
                            <button class="btn ibutton-grey ibutton-reg-reset" name="resetOrder" type="button" onclick="eventHandler.goToReferrerUrl()">
                                Отмена
                            </button>
                        </td>
                        <td>
                            <button class="btn ibutton-orange ibutton-registration" name="acceptOrder" type="submit">
                                Зарегистрироваться
                            </button>
                        </td>
                    </tr>

                </tbody>
            </table>
        </form>
    </div>
</div>
