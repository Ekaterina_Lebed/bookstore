
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title></title>
</head>
<body>
    <div class="content-header">
        Регистрация успешна
    </div>

    Вы зарегистрированы и успешно авторизовались.
    <p></p>
    <security:authorize access="isAuthenticated()">
        Перейти на
        <a class="books-section-path" href="<c:url value="/profile"/>">
            Мой профиль
        </a>
    </security:authorize>

</body>
</html>
