package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.model.catalog.BookSet;
import com.lea.books.model.ui.Breadcrumb;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.catalog.CategoryService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

//import static com.lea.books.controller.catalog.AnyObjectBuilder.*;
import static org.mockito.Mockito.*;

public class CategoryControllerTest {
    @Mock
    CategoryService categoryService;

    @Mock
    BookService bookService;

    @Mock
    ResourceUtils resourceUtils;

    @Mock
    BreadcrumbsBuilder breadcrumbsBuilder;

    @InjectMocks
    CategoryController categoryController;

    @Spy
    ExtendedModelMap model = new ExtendedModelMap();

    private final String VIEW_NAME_LIST = "view.bookList";
    private final String VIEW_NAME_SETS = "view.bookSets";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void showBookList(){
        Category aCategory = mock(Category.class);
        @SuppressWarnings("unchecked")
        List<Book> aBookList = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        when(categoryService.selectById(1)).thenReturn(aCategory);
        when(categoryService.selectById(0)).thenReturn(null);
        when(categoryService.selectById(-1)).thenReturn(null);
        when(categoryService.selectById(10999)).thenReturn(null);
        when(bookService.selectByCategory(aCategory)).thenReturn(aBookList);
        when(breadcrumbsBuilder.build(aCategory)).thenReturn(aBreadcrumbs);

        model.clear();
        Assert.assertEquals(categoryController.showBookList("1", model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("section"),aCategory);
        Assert.assertEquals(model.get("bookList"),aBookList);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);
        verify(categoryService,atLeastOnce()).selectById(1);
        verify(bookService,atLeastOnce()).selectByCategory(aCategory);
        verify(breadcrumbsBuilder, atLeastOnce()).build(aCategory);

        //-----------------------------------------------------------
        //STEP_2: if ID not valid
        //-----------------------------------------------------------
        showBookPage_TestIgnoredId("0", 0, VIEW_NAME_LIST);
        showBookPage_TestIgnoredId("-1", -1, VIEW_NAME_LIST);
        showBookPage_TestIgnoredId("12cats", 0, VIEW_NAME_LIST);
    }

    @Test
    public void showBookListCategoryNovelty(){
        Category aCategory = mock(Category.class);
        @SuppressWarnings("unchecked")
        List<Book> aBookList = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        when(categoryService.noveltyCategory()).thenReturn(aCategory);
        when(bookService.selectByCategory(aCategory)).thenReturn(aBookList);
        when(breadcrumbsBuilder.build(aCategory)).thenReturn(aBreadcrumbs);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryNovelty(model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("section"),aCategory);
        Assert.assertEquals(model.get("bookList"),aBookList);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);
        verify(categoryService,atLeastOnce()).noveltyCategory();
        verify(bookService,atLeastOnce()).selectByCategory(aCategory);
        verify(breadcrumbsBuilder,atLeastOnce()).build(aCategory);
        Assert.assertEquals(model.size(),3);

        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbsN = mock(ArrayList.class);
        when(breadcrumbsBuilder.build()).thenReturn(aBreadcrumbsN);

        model.clear();
        when(categoryService.noveltyCategory()).thenReturn(null);
        Assert.assertEquals(categoryController.showBookListCategoryNovelty(model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbsN);
        verify(breadcrumbsBuilder,atLeastOnce()).build();
        Assert.assertEquals(model.size(),1);

    }

    @Test
    public void showBookListCategoryTopSales(){
        Category aCategory = mock(Category.class);
        @SuppressWarnings("unchecked")
        List<Book> aBookList = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbsN2 = mock(ArrayList.class);

        when(categoryService.topSalesCategory()).thenReturn(aCategory);
        when(bookService.selectByCategory(aCategory)).thenReturn(aBookList);
        when(breadcrumbsBuilder.build(aCategory)).thenReturn(aBreadcrumbs);
        when(breadcrumbsBuilder.build()).thenReturn(aBreadcrumbsN2);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryTopSales(model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("section"),aCategory);
        Assert.assertEquals(model.get("bookList"),aBookList);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(bookService,atLeastOnce()).selectByCategory(aCategory);
        verify(breadcrumbsBuilder,atLeastOnce()).build(aCategory);
        Assert.assertEquals(model.size(),3);

        model.clear();
        when(categoryService.topSalesCategory()).thenReturn(null);
        Assert.assertEquals(categoryController.showBookListCategoryTopSales(model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbsN2);
        verify(breadcrumbsBuilder,atLeastOnce()).build();
        verify(bookService,never()).selectByCategory(null);
        Assert.assertEquals(model.size(),1);
    }

    @Test
    public void showBookListCategoryRecommended(){
        Category aCategoryN = mock(Category.class);
        Category aCategoryT = mock(Category.class);

        List<Category> aCategories = new ArrayList<>();
        @SuppressWarnings("unchecked")
        List<BookSet> aBookSets = mock(ArrayList.class);

        //-----------------------------------------------------------
        //STEP_1
        //-----------------------------------------------------------
        aCategories.clear();
        aCategories.add(aCategoryN);
        aCategories.add(aCategoryT);

        reset(categoryService);
        reset(bookService);
        when(categoryService.noveltyCategory()).thenReturn(aCategoryN);
        when(categoryService.topSalesCategory()).thenReturn(aCategoryT);
        when(bookService.buildBookSets(aCategories,10)).thenReturn(aBookSets);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryRecommended(model), VIEW_NAME_SETS);
        Assert.assertEquals(model.get("bookSets"), aBookSets);
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(bookService,atLeastOnce()).buildBookSets(aCategories,10);

        //-----------------------------------------------------------
        //STEP_2
        //-----------------------------------------------------------
        aCategories.clear();
        aCategories.add(aCategoryT);

        reset(categoryService);
        reset(bookService);
        when(categoryService.noveltyCategory()).thenReturn(null);
        when(categoryService.topSalesCategory()).thenReturn(aCategoryT);
        when(bookService.buildBookSets(aCategories,10)).thenReturn(aBookSets);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryRecommended(model), VIEW_NAME_SETS);
        Assert.assertEquals(model.get("bookSets"), aBookSets);
        Assert.assertEquals(model.size(), 1);
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(categoryService,atLeastOnce()).noveltyCategory();
        verify(bookService,atLeastOnce()).buildBookSets(aCategories,10);

        //-----------------------------------------------------------
        //STEP_3
        //-----------------------------------------------------------
        aCategories.clear();
        aCategories.add(aCategoryN);

        reset(categoryService);
        reset(bookService);
        when(categoryService.noveltyCategory()).thenReturn(aCategoryN);
        when(categoryService.topSalesCategory()).thenReturn(null);
        when(bookService.buildBookSets(aCategories,10)).thenReturn(aBookSets);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryRecommended(model), VIEW_NAME_SETS);
        Assert.assertEquals(model.get("bookSets"),aBookSets);
        Assert.assertEquals(model.size(),1);
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(categoryService,atLeastOnce()).noveltyCategory();
        verify(bookService,atLeastOnce()).buildBookSets(aCategories,10);

        //-----------------------------------------------------------
        //STEP_4
        //-----------------------------------------------------------
        aCategories.clear();
        reset(categoryService);
        reset(bookService);
        when(categoryService.noveltyCategory()).thenReturn(null);
        when(categoryService.topSalesCategory()).thenReturn(null);
        when(bookService.buildBookSets(aCategories,10)).thenReturn(aBookSets);

        model.clear();
        Assert.assertEquals(categoryController.showBookListCategoryRecommended(model), VIEW_NAME_SETS);
        verify(categoryService,atLeastOnce()).topSalesCategory();
        verify(categoryService,atLeastOnce()).noveltyCategory();
        verify(bookService,never()).buildBookSets(aCategories,10);
        Assert.assertEquals(model.size(),0);
    }

    private void showBookPage_TestIgnoredId(String ID,int id,String PAGE){
        model.clear();
        Assert.assertEquals(categoryController.showBookList(ID, model), PAGE);
        verify(bookService, never()).selectById(id);
        Assert.assertEquals(model.size(),0);
    }
}

