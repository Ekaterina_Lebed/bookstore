package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.model.ui.Breadcrumb;
import com.lea.books.service.catalog.BookService;
import com.lea.books.utils.ResourceUtils;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class BookControllerTest {
    @Mock
    BookService bookService;

    @Mock
    BreadcrumbsBuilder breadcrumbsBuilder;

    @InjectMocks
    BookController bookController;

    @Mock
    ResourceUtils resourceUtils;

    @Spy
    List<Book> bookList=new ArrayList<>();

    @Spy
    ExtendedModelMap model = new ExtendedModelMap();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    private static final String PAGE = "view.book";
    private static final String PAGE_ERROR = "error.catalog";

    @Test
    public void showBookPage(){
        Book aBook = mock(Book.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        when(bookService.selectById(1)).thenReturn(aBook);
        when(bookService.selectById(100000)).thenReturn(null);
        when(breadcrumbsBuilder.build(aBook)).thenReturn(aBreadcrumbs);

        //-----------------------------------------------------------
        //STEP_1: successful
        //-----------------------------------------------------------
        model.clear();
        Assert.assertEquals(bookController.showBookPage("1", model), PAGE);
        Assert.assertEquals(model.get("book"),aBook);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);
        Assert.assertEquals(model.size(),2);
        verify(bookService, atLeastOnce()).selectById(1);
        verify(breadcrumbsBuilder, atLeastOnce()).build(aBook);

        //-----------------------------------------------------------
        //STEP_2: if ID not valid
        //-----------------------------------------------------------
        showBookPage_TestIgnoredId("0", 0, PAGE_ERROR);
        showBookPage_TestIgnoredId("-1", -1, PAGE_ERROR);
        showBookPage_TestIgnoredId("12asd", 0, PAGE_ERROR);

        //-----------------------------------------------------------
        //STEP_3: if not exist
        //-----------------------------------------------------------
        model.clear();
        Assert.assertEquals(bookController.showBookPage("100000", model),PAGE_ERROR);
        verify(bookService, atLeastOnce()).selectById(100000);
        Assert.assertEquals(model.size(),0);
    }

    void showBookPage_TestIgnoredId(String ID,int id,String PAGE){
        model.clear();
        Assert.assertEquals(bookController.showBookPage(ID, model), PAGE);
        verify(bookService, never()).selectById(id);
        Assert.assertEquals(model.size(),0);
    }
}

