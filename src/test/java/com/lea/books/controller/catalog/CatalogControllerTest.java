package com.lea.books.controller.catalog;

import com.lea.books.model.ui.Breadcrumb;
import com.lea.books.service.catalog.CatalogService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import com.lea.books.model.catalog.CatalogBranch;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

public class CatalogControllerTest {
    @Mock
    CatalogService catalogService;

    @Mock
    BreadcrumbsBuilder breadcrumbsBuilder;

    @InjectMocks
    CatalogController catalogController;

    @Spy
    ExtendedModelMap model = new ExtendedModelMap();

    private final String PAGE = "view.catalog";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void showCatalog(){
        @SuppressWarnings("unchecked")
        List<CatalogBranch> aBranches = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        //-----------------------------------------------------------
        //STEP_1: successful
        //-----------------------------------------------------------
        reset(catalogService);
        reset(breadcrumbsBuilder);
        when(catalogService.buildBranches(3)).thenReturn(aBranches);
        when(breadcrumbsBuilder.build()).thenReturn(aBreadcrumbs);
        when(aBranches.isEmpty()).thenReturn(false);

        model.clear();
        Assert.assertEquals(catalogController.showCatalog(model), PAGE);
        Assert.assertEquals(model.get("catalogTree"), aBranches);
        Assert.assertEquals(model.get("sectionPath"), aBreadcrumbs);
        Assert.assertEquals(model.size(),2);
    }
}
