package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.model.ui.Breadcrumb;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

//import static com.lea.books.controller.catalog.AnyObjectBuilder.*;
import static org.mockito.Mockito.*;

public class SearchControllerTest {
    @Mock
    BookService bookService;

    @Mock
    BreadcrumbsBuilder breadcrumbsBuilder;

    @Mock
    ResourceUtils resourceUtils;

    @InjectMocks
    SearchController controller;

    @Spy
    ExtendedModelMap model = new ExtendedModelMap();

    private static final String VIEW_NAME_LIST = "view.bookList";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void SearchTest(){
        @SuppressWarnings("unchecked")
        List<Book> aBooks = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        when(bookService.search("test_name")).thenReturn(aBooks);
        when(breadcrumbsBuilder.build()).thenReturn(aBreadcrumbs);

        model.clear();
        Assert.assertEquals(controller.search("test_name",model),VIEW_NAME_LIST);
        Assert.assertEquals(model.get("section"),null);
        Assert.assertEquals(model.get("bookList"),aBooks);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);

        verify(bookService,atLeastOnce()).search("test_name");
        verify(breadcrumbsBuilder,atLeastOnce()).build();
    }
}
