package com.lea.books.controller.catalog;

import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Section;
import com.lea.books.model.ui.Breadcrumb;
import com.lea.books.service.catalog.BookService;
import com.lea.books.service.catalog.SectionService;
import com.lea.books.service.ui.BreadcrumbsBuilder;
import com.lea.books.utils.ResourceUtils;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.ui.ExtendedModelMap;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class SectionControllerTest {
    @Mock
    SectionService sectionService;

    @Mock
    BookService bookService;

    @Mock
    ResourceUtils resourceUtils;

    @Mock
    BreadcrumbsBuilder breadcrumbsBuilder;

    @InjectMocks
    SectionController sectionController;

    @Spy
    ExtendedModelMap model = new ExtendedModelMap();

    private static final String VIEW_NAME_LIST = "view.bookList";

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void showBookList(){
        Section aSection = mock(Section.class);
        @SuppressWarnings("unchecked")
        List<Book> aBooks = mock(ArrayList.class);
        @SuppressWarnings("unchecked")
        List<Breadcrumb> aBreadcrumbs = mock(ArrayList.class);

        reset(sectionService);
        reset(bookService);
        when(sectionService.selectSectionById(100)).thenReturn(aSection);
        when(bookService.selectBySection(aSection)).thenReturn(aBooks);
        when(breadcrumbsBuilder.build(aSection)).thenReturn(aBreadcrumbs);

        //-----------------------------------------------------------
        //STEP_1
        //-----------------------------------------------------------
        model.clear();
        Assert.assertEquals(sectionController.showBookList("100", model), VIEW_NAME_LIST);
        Assert.assertEquals(model.get("section"),aSection);
        Assert.assertEquals(model.get("bookList"),aBooks);
        Assert.assertEquals(model.get("sectionPath"),aBreadcrumbs);
        Assert.assertEquals(model.size(),3);
        verify(sectionService,atLeastOnce()).selectSectionById(100);
        verify(bookService,atLeastOnce()).selectBySection(aSection);
        verify(breadcrumbsBuilder,atLeastOnce()).build(aSection);

        //-----------------------------------------------------------
        //STEP_2
        //-----------------------------------------------------------
        showBookList_TestIgnoredId("0", 0, VIEW_NAME_LIST);
        showBookList_TestIgnoredId("-1", -1, VIEW_NAME_LIST);
        showBookList_TestIgnoredId("12point", 0, VIEW_NAME_LIST);

//        //-----------------------------------------------------------
//        //STEP_3: if not exist
//        //-----------------------------------------------------------
//        model.clear();
//        Assert.assertEquals(sectionController.showBookList("10999", model),VIEW_NAME_LIST);
//        verify(sectionService,atLeastOnce()).selectSectionById(10999);
//        Assert.assertEquals(model.size(),3);
//        Assert.assertEquals(model.get("section"),null);
//        Assert.assertEquals(model.get("bookList"),anyBookList(0));
//        Assert.assertEquals(model.get("sectionPath"),anyBreadcrumbs(0));
    }

    void showBookList_TestIgnoredId(String ID,int id,String PAGE){
        model.clear();
        Assert.assertEquals(sectionController.showBookList(ID, model), PAGE);
        Assert.assertEquals(model.size(),0);
        verify(sectionService,never()).selectSectionById(id);
        verify(bookService,never()).selectBySection(null);
        verify(breadcrumbsBuilder,never()).build((Section)null);
    }
}
