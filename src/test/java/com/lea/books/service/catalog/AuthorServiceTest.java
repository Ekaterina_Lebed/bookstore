package com.lea.books.service.catalog;


import com.lea.books.dao.api.AuthorDao;
import com.lea.books.entity.catalog.Author;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class AuthorServiceTest {
    @Mock
    AuthorDao dao;

    @InjectMocks
    AuthorService service;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void insert(){
        doNothing().when(dao).insert(any(Author.class));
        service.insert(any(Author.class));
        verify(dao, atLeastOnce()).insert(any(Author.class));

        doThrow(Exception.class).when(dao).insert(any(Author.class));
        service.insert(any(Author.class));
    }

    @Test
    public void update(){
        doNothing().when(dao).update(any(Author.class));
        service.update(any(Author.class));
        verify(dao, atLeastOnce()).update(any(Author.class));

        doThrow(Exception.class).when(dao).update(any(Author.class));
        service.update(any(Author.class));
    }
}
