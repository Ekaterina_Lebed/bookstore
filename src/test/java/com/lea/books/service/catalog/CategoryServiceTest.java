package com.lea.books.service.catalog;


import com.lea.books.dao.api.CategoryDao;
import com.lea.books.entity.catalog.Category;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class CategoryServiceTest {
    @Mock
    CategoryDao dao;

    @Mock
    Category noveltyCategory;

    @Mock
    Category topSalesCategory;

    @InjectMocks
    CategoryService service;

    @Spy
    private List<Category> list = new ArrayList<>();


    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void insert(){
        doNothing().when(dao).insert(any(Category.class));
        service.insert(any(Category.class));
        verify(dao,atLeastOnce()).insert(any(Category.class));

        doThrow(Exception.class).when(dao).insert(any(Category.class));
        service.insert(any(Category.class));
    }

    @Test
    public void update(){
        doNothing().when(dao).update(any(Category.class));
        service.update(any(Category.class));
        verify(dao,atLeastOnce()).update(any(Category.class));

        doThrow(Exception.class).when(dao).update(any(Category.class));
        service.update(any(Category.class));
    }

    @Test
    public void selectById() {
        Category categoryObj = mock(Category.class);
        when(dao.selectById(anyInt())).thenReturn(categoryObj);
        service.selectById(anyInt());
        Assert.assertEquals(service.selectById(categoryObj.getId()), categoryObj);
        verify(dao, atLeastOnce()).selectById(anyInt());
    }

    @Test
    public void noveltyCategory(){
        Assert.assertEquals(service.noveltyCategory(),noveltyCategory);
    }

    @Test
    public void topSalesCategory(){
        Assert.assertEquals(service.topSalesCategory(),topSalesCategory);
    }
}
