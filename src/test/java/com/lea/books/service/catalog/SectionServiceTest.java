package com.lea.books.service.catalog;


import com.lea.books.dao.api.SectionDao;

import com.lea.books.entity.catalog.Section;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

public class SectionServiceTest {
    @Mock
    SectionDao dao;

    @InjectMocks
    SectionService service;

    @Spy
    private List<Section> list = new ArrayList<>();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        list.add(getSection(1, "123-as-gh"));
        list.add(getSection(2, "ppp-as-gh"));
    }

    @Test
    public void insert(){
        doNothing().when(dao).insert(any(Section.class));
        service.insert(any(Section.class));
        verify(dao, atLeastOnce()).insert(any(Section.class));

        doThrow(Exception.class).when(dao).insert(any(Section.class));
        service.insert(any(Section.class));
    }

    @Test
    public void update(){
        doNothing().when(dao).update(any(Section.class));
        service.update(any(Section.class));
        verify(dao, atLeastOnce()).update(any(Section.class));

        doThrow(Exception.class).when(dao).update(any(Section.class));
        service.update(any(Section.class));
    }

    @Test
    public void selectSectionById(){
        Section section = mock(Section.class);
        when(dao.selectById(anyInt())).thenReturn(section);
        when(dao.selectById(0)).thenReturn(null);
        when(dao.selectById(-1)).thenReturn(null);

        Assert.assertEquals(service.selectSectionById(1), section);
        Assert.assertEquals(service.selectSectionById(2),section);
        Assert.assertNull(service.selectSectionById(0));
        Assert.assertNull(service.selectSectionById(-1));
    }

    @Test
    public void selectByParent(){
        Section section = mock(Section.class);
        when(dao.selectByParent(section)).thenReturn(list);
        Assert.assertEquals(service.selectByParent(section), list);
    }

    private Section getSection(int id, String name){
        Section section = new Section();
        section.setId(id);
        section.setName(name);

        return section;
    }
}
