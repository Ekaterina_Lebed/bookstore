package com.lea.books.service.catalog;

import com.lea.books.dao.api.BookDao;
import com.lea.books.dao.api.SectionDao;
import com.lea.books.entity.catalog.Book;
import com.lea.books.entity.catalog.Category;
import com.lea.books.entity.catalog.Section;
import com.lea.books.model.catalog.BookSet;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class BookServiceTest {
    @Mock
    SectionDao sectionDao;

    @Mock
    BookDao bookDao;

    @InjectMocks
    BookService bookService;

    @Spy
    List<Book> list = new ArrayList<>();

    @Spy
    List<Section> sections = new ArrayList<>();

    @Spy
    List<Category> categories = new ArrayList<>();

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        categories.add(new Category());
    }

    @Test
    public void insert(){
        doNothing().when(bookDao).insert(any(Book.class));
        bookService.insert(any(Book.class));
        verify(bookDao,atLeastOnce()).insert(any(Book.class));

        doThrow(Exception.class).when(bookDao).insert(any(Book.class));
        bookService.insert(any(Book.class));
    }

    @Test
    public void update(){
        doNothing().when(bookDao).update(any(Book.class));
        bookService.update(any(Book.class));
        verify(bookDao,atLeastOnce()).update(any(Book.class));

        doThrow(Exception.class).when(bookDao).update(any(Book.class));
        bookService.update(any(Book.class));

    }

    @Test
    public void selectByCategory(){
        when(bookDao.selectByCategory(any(Category.class))).thenReturn(list);
        Assert.assertEquals(bookService.selectByCategory(any(Category.class)),list);
        verify(bookDao,atLeastOnce()).selectByCategory(any(Category.class));
    }

    @Test
    public void selectByCategoryAndCount(){
        when(bookDao.selectByCategory(any(Category.class),anyInt())).thenReturn(list);
        Assert.assertEquals(bookService.selectByCategory(any(Category.class),anyInt()),list);
        verify(bookDao,atLeastOnce()).selectByCategory(any(Category.class),anyInt());
    }

    @Test
    public void selectBySection(){
        when(sectionDao.selectBySection(any(Section.class))).thenReturn(sections);
        Assert.assertEquals(bookService.selectBySection(any(Section.class)),list);
        verify(bookDao,atLeastOnce()).selectBySections(sections);
    }

    @Test
    public void search(){
        when(bookDao.selectByName(anyString())).thenReturn(list);
        Assert.assertEquals(bookService.search(anyString()), list);
    }

    @Test
    public void selectById(){
        Book anyBook = mock(Book.class);
        when(bookDao.selectById(anyInt())).thenReturn(anyBook);
        Assert.assertEquals(bookService.selectById(anyInt()),anyBook);
        verify(bookDao,atLeastOnce()).selectById(anyInt());
    }

    @Test
    public void buildBookSets(){
        BookSet item = new BookSet();
        item.setCategory(mock(Category.class));
        item.setBooks(list);

        List<BookSet> bookSets = new ArrayList<>();
        bookSets.add(item);

        bookService.buildBookSets(categories,10);
        when(bookService.selectByCategory(categories.get(0),10)).thenReturn(list);
        verify(bookDao,atLeastOnce()).selectByCategory(categories.get(0),10);
    }
}
