
<%@ page contentType="text/html;charset=UTF-8" language="java"%>

<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<div class="content-header">
    Мой кабинет
</div>

<div class="profile-user">
    <tiles:insertAttribute name="profileMenu"/>
    <tiles:insertAttribute name="profileItemContent"/>
</div>