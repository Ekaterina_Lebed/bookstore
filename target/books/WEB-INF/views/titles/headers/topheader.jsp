
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>


<div class="topheader-section">

    <div class="topheader-navigation">
        <ul class="hmenu2">
            <li>
                <a href="<c:url value="/aboutus"/>">
                    <span class="hmenu2-action-topheader">О нас</span>
                </a>
            </li>

            <li>
                <a href="<c:url value="/delivery"/>">
                    <span class="hmenu2-action-topheader">Доставка и оплата</span>
                </a>
            </li>

            <li>
                <a href="<c:url value="/help"/>">
                    <span class="hmenu2-action-topheader">Помощь</span>
                </a>
            </li>

            <li>
                <a href="<c:url value="/contacts"/>">
                    <span class="hmenu2-action-topheader">Контакты</span>
                </a>
            </li>
        </ul>
    </div>

    <div class="topheader-user">
        <ul class="hmenu2">
            <security:authorize access="isAuthenticated()">
                <li>
                    <a href="<c:url value="/profile"/>">
                        <span class="hmenu2-action-topheader">
                            <security:authentication property="principal.username"/>
                        </span>
                    </a>
                </li>

                <li>
                    <a href="<c:url value="/logout"/>">
                        <span class="hmenu2-action-topheader">Выход</span>
                    </a>
                </li>
            </security:authorize>

            <security:authorize access="isAnonymous()">
                <li>
                    <a href="<c:url value="/user/registration"/>">
                        <span class="hmenu2-action-topheader">Регистрация</span>
                    </a>
                </li>
                <li>
                    <a href="<c:url value="/login"/>">
                        <span class="hmenu2-action-topheader">Вход в магазин</span>
                    </a>
                </li>
            </security:authorize>
        </ul>
    </div>
</div>

