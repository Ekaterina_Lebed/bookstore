
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<div class="footer">
    <div class="footer-column">
        <ul>
            <h1>Book Store</h1>
            <li><a href="<c:url value="/contacts"/>">Контакты</a></li>
            <li><a href="<c:url value="/delivery"/>">Доставка</a></li>
            <li><a href="<c:url value="/payment"/>">Оплата</a></li>
            <li><a href="<c:url value="/return"/>">Возврат</a></li>
        </ul>
    </div>
    <div class="footer-column">
        <ul>
            <h1>Помощь</h1>
            <li><a href="<c:url value="/help"/>">Личный кабинет</a></li>
            <li><a href="<c:url value="/partnership"/>">Бонусная программа</a></li>
            <li><a href="<c:url value="/loyalty"/>">Партнерская программа</a></li>
            <li><a href="<c:url value="/help"/>">Помощь</a></li>
        </ul>
        </div>
</div>

