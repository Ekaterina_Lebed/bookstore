
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="hmenu">
    <div class="menu-item ${SELECT_ProfilePersonal}" id="ProfilePersonal">
        <a href="<c:url value="/profile/personal"/>">Мои настройки</a>
    </div>

    <div class="menu-item ${SELECT_ProfileOrders}" id="ProfileOrders">
        <a href="<c:url value="/profile/orders"/>">Мои заказы</a>
    </div>

    <security:authorize access="hasRole('ROLE_ADMIN')">
    <div class="menu-item ${SELECT_ProfileAdminOrders}" id="ProfileAdminOrders">
        <a href="<c:url value="/admin/orders"/>">Все заказы</a>
    </div>
    </security:authorize>
</div>