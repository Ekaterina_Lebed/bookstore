
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="hmenu-content">
    <div class="profile-item item-h1">
        <div class="c1">Имя</div>
        <div class="c2">${user.name}</div>
        <div class="clear_float"></div>
    </div>

    <div class="profile-item item-h1">
        <div class="c1">Фамилия</div>
        <div class="c2">${user.lastName}</div>
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Электронная почта</div>
        <div class="c2">${user.email}</div>
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Телефон</div>
        <div class="c2">${user.phone}</div>
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Город</div>
        <div  class="c2">${user.city}</div >
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Улица</div>
        <div  class="c2">${user.street}</div >
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Дом</div>
        <div  class="c2">${user.house}</div >
        <div class="clear_float"></div>
    </div>

    <div class="profile-item">
        <div class="c1">Квартира</div>
        <div  class="c2">${user.flat}</div >
        <div class="clear_float"></div>
    </div>
</div>

