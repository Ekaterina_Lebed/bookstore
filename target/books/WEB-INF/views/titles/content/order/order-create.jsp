
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>



<script type="text/javascript" src="<c:url value="/resources/script/validator/ValueValidator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/script/validator/OrderValidator.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/script/prompt/OrderPrompt.js"/>"></script>

<div class="content-header">
    Оформление заказа
</div>

<div class="content-body">
    <div class="content-body-left">
        <form method="POST" action="<c:url value="/order/accept"/>" onsubmit="return OrderValidator.validate(this.form);">

            <table class="order-fields">
                <tbody>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderUserName" >Имя и фимилия<span class="imp">*</span></label>
                    </td>
                    <td class="iField">
                        <input class="elem input-elem"
                               id="orderUserName" name="orderUserName"
                               type="text" value="${orderUser.name}">
                        <label for="orderUserName" class="error-info"></label>

                        <input class="elem input-elem"
                               id="orderUserLastName" name="orderUserLastName"
                               type="text" value="${orderUser.lastName}">
                        <label for="orderUserLastName" class="error-info" ></label>
                    </td>
                </tr>

                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderPhone">Телефон<span class="imp">*</span></label>
                    </td>
                    <td class="iField">
                        <input class="elem input-elem"
                               id="orderPhone" name="orderPhone"
                               type="text" value="${orderContact.phone}">
                        <label for="orderPhone" class="error-info"></label>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderEmail">Электронная почта<span class="imp">*</span></label>
                    </td>
                    <td class="iField">
                        <input class="elem input-elem"
                               id="orderEmail" name="orderEmail"
                               type="email" value="${orderContact.email}">
                        <label class="error-info"></label>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderCity">Город</label>
                    </td>
                    <td class="iField">
                        <input class="elem input-elem"
                               id="orderCity" name="orderCity"
                               type="text" value="${orderContact.city}">
                        <label for="orderCity" class="error-info"></label>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderCity">Адрес получателя</label>
                    </td>
                    <td class="iField">
                        <div class="order__str">
                            <input class="elem input-elem"
                                   id="orderStreet" name="orderStreet"
                                   type="text" value="${orderContact.street}" >
                            <label for="orderStreet" class="error-info"></label>
                        </div>
                        <div class="order__bld">
                            <input class="elem input-elem"
                                   id="orderHouse" name="orderHouse"
                                   type="text" value=<c:out value="${orderContact.house}"/>>
                            <label for="orderHouse" class="error-info"></label>
                        </div>
                        <div class="order__apart">
                            <input class="elem input-elem"
                                   id="orderFlat" name="orderFlat"
                                   type="text" value=<c:out value="${orderContact.flat}"/>>
                            <label for="orderFlat" class="error-info"></label>
                        </div>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderDelivery">Способ доставки</label>
                    </td>
                    <td class="iField">
                        <select class="elem select-elem" id="orderDelivery" name="orderDelivery">
                            <c:forEach var="item"  items="${deliveryList}">
                                <option value=${item.id}>${item.name}</option>
                            </c:forEach>
                        </select>
                        <label class="error-info"></label>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderPayment">Оплата</label>
                    </td>
                    <td class="iField">
                        <select class="elem select-elem" id="orderPayment" name="orderPayment">
                            <c:forEach var="item"  items="${paymentList}">
                                <option value=${item.id}>${item.name}</option>
                            </c:forEach>
                        </select>
                        <label class="error-info"></label>
                    </td>
                </tr>
                <tr class="order-field">
                    <td class="iLabel">
                        <label for="orderComment">Пожелания к заказу</label>
                    </td>
                    <td class="iField">
                        <textarea class="elem text-elem" id="orderComment" name="orderInfo">
                        </textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a class="ibutton-goto-shoping" onclick="eventHandler.goToReferrerUrl()">
                            Вернуться к покупкам
                        </a>
                    </td>
                    <td>
                        <button class="ibutton-green ibutton-order-submit" type="submit" name="acceptOrder">
                            Оформить заказ
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </form>
    </div>

    <div class="content-body-right">
        <div class="cart-items" id="cart-items">
            <c:forEach var="cartItem" items="${cart.items}">
                <div class="cart-item" id="cartItem[${cartItem.book.id}]">
                    <div class="cart-item-icon">
                        <img class="book-img100" src="<c:url value="${cartItem.book.imageSrc}"/>" border="0" align="">
                    </div>

                    <div class="cart-item-body">
                        <div class="title item-h1">
                            ${cartItem.book.name}
                        </div>

                        <div class="info item-h1">
                            <span class="price item-sum">
                                <fmt:formatNumber type="number" minFractionDigits="2" value="${cartItem.price}"/>
                                <em>UAH</em>
                            </span>
                            <span class="count">
                                <button class="ibutton-increase" onclick="cartEventHandler.reduceCount('${cartItem.book.id}')"></button>
                                <input  class="input-count" id="itemCount[${cartItem.book.id}]" type="text" value="${cartItem.count}"
                                        onchange="cartEventHandler.setCount('${cartItem.book.id}')">
                                <button class="ibutton-reduce" onclick="cartEventHandler.increaseCount('${cartItem.book.id}')"></button>
                            </span>
                            <span class="sum item-sum">
                                <span id="itemSum[${cartItem.book.id}]">
                                    <fmt:formatNumber type="number" minFractionDigits="2" value="${cartItem.sum}"/>
                                </span>
                                <em>UAH</em>
                            </span>
                        </div>
                    </div>

                    <div class="command">
                        <div class="ibutton-delete" onclick="cartEventHandler.deleteItem('${cartItem.book.id}')">
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <div class="total-item">
            <div class="cart-item-icon">
            </div>

            <div class="cart-item-body">
                <span class="sum">
                    Итого:
                    <span class="item-sum-total">
                        <span id="totalSum">
                            <fmt:formatNumber type="number" minFractionDigits="2" value="${cart.sum}"/>
                        </span>
                        <em>UAN</em>
                    </span>
                </span>
            </div>
        </div>
    </div>

    <script>
        OrderPrompt.showPrompts();
    </script>
</div>