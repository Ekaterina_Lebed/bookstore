
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="hmenu-content">
    <c:forEach var="order" items="${orderList}">
        <div class="list-item-short">
            <div class="column1">
                <a href="<c:url value="/order/edit?id=${order.id}"/>">
                    <div class="title item-h0">${order.name}</div>
                </a>
                <div class="sum item-h1">Сумма: ${order.sum}</div>
            </div>
            <div class="column2  item-h1">
                <div class="info">Статус: ${order.status}</div>
            </div>
            <div class="clear_float"></div>
        </div>
    </c:forEach>
</div>