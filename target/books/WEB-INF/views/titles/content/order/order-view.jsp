
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="main-content">

    <div class="header">Заказ №1464372124</div>

    <div class="content">
        <table class="list">
            <thead>
            <tr>
                <td class="left" colspan="2">Детали заказа</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="left" style="width: 50%;">
                    <b>№ Заказа:</b> ${order.id}<br>
                    <b>Дата:</b> ${order.date}<br>
                    <b>Получатель:</b>${order.user.name}</td>
                <td class="left" style="width: 50%;">
                    <b>Способ оплаты:</b> ${order.payment.name}<br>
                    <b>Способ доставки:</b> ${order.delivery.name}</td>
            </tr>
            </tbody>
        </table>

        <table class="list">
            <thead>
            <tr>
                <td class="left">Адрес доставки:</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="left">
                ${order.street}, д. ${order.house}, кв. ${order.flat}<br>
                ${order.city}</td>
            </tr>
            </tbody>
        </table>

        <table class="list">
            <thead>
            <tr>
                <td class="left">Наименование товара</td>
                <td class="right">Количество</td>
                <td class="right">Цена</td>
                <td class="right">Итого</td>
            </tr>
            </thead>
            <tbody>
            <c:forEach var="orederItem" items="${order.items}">
                <tr>
                    <td class="left">${orederItem.book.name}</td>
                    <td class="right">${orederItem.count}</td>
                    <td class="right">
                        <fmt:formatNumber type="number" minFractionDigits="2" value="${orederItem.price}"/>
                        <em>UAH</em>
                    </td>
                    <td class="right">
                        <fmt:formatNumber type="number" minFractionDigits="2" value="${orederItem.sum}"/>
                        <em>UAH</em>
                    </td>
                </tr>
            </c:forEach>
            </tbody>

            <tfoot>
            <tr>
                <td colspan="2"></td>
                <td class="right"><b>Итого:</b></td>
                <td class="right">
                    <fmt:formatNumber type="number" minFractionDigits="2" value="${oreder.sum}"/>
                    <em>UAH</em>
                </td>
                <td></td>
            </tr>
            </tfoot>
        </table>

        <table class="list">
            <thead>
            <tr>
                <td class="left">Комментарий к заказу</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="left">${oreder.info}<br><br></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>




