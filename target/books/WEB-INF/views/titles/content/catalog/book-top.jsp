<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div class="main-content__section">
    <c:forEach var="bookSet" items="${bookSets}">
        <div class="heading">
            <span>${bookSet.group.name}</span>
        </div>
        <span class="more-link">
            <a href="<c:url value="/category?id=${bookSet.group.id}"/>">
                Весь список
            </a>
        </span>

        <div class="scrollWrapper">
            <div class="scrollableArea">
                <c:forEach var="book" items="${bookSet.books}">
                    <div class="book-item">
                        <div class="item-image">
                            <a href="<c:url value="/book?id=${book.id}"/>">
                                <div class="item-image-view">
                                    <img src="<c:url value="${book.imageSrc}"/>" alt="${book.name}">
                                </div>
                            </a>
                        </div>

                        <div class="item-content">
                            <a href="<c:url value="/book?id=${book.id}"/>">
                                <div class="title item-h1">${book.name}</div>
                            </a>

                            <span class="price">
                                <fmt:formatNumber type="number" minFractionDigits="2" value="${book.price}"/>
                                <em>${currencyApp}</em>
                            </span>


                            <button class="btnquickbuy" type="submit" name="buyfromcatalog" onclick="cartEventHandler.addItem('${book.id}')">
                            </button>
                        </div>
                    </div>
                </c:forEach>
            </div>
        </div>
    </c:forEach>
</div>