<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>

<div class="catalog-section">
    <c:forEach var="catalogTree" items="${catalogTrees}">
        <div class="catalog-tree">
            <c:forEach var="CatalogBranch_h0" items="${catalogTree}">
                <div class="catalog-section-h0">
                    <a href="<c:url value="/section?id=${CatalogBranch_h0.section.id}"/>">
                            ${CatalogBranch_h0.section.name}
                    </a>
                </div>

                <ul class="catalog-section-h1">
                    <c:forEach var="h1CatalogBranch" items="${CatalogBranch_h0.subBranches}">
                        <li>
                            <a href="<c:url value="/section?id=${h1CatalogBranch.section.id}"/>">
                                ${h1CatalogBranch.section.name}
                            </a>
                        </li>

                        <ul class="catalog-section-h2">

                            <c:forEach var="h2CatalogBranch" items="${h1CatalogBranch.subBranches}">
                                <li>
                                    <a href="<c:url value="/section?id=${h2CatalogBranch.section.id}"/>">
                                        ${h2CatalogBranch.section.name}
                                    </a>
                                </li>
                            </c:forEach>
                        </ul>
                    </c:forEach>
                </ul>
            </c:forEach>
        </div>
    </c:forEach>
</div>


