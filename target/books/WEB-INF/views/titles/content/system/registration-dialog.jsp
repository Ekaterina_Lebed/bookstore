<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="content-header">
    Создать учетную запись
</div>

<div class="content-body">
    <div class="content-body-left2">
        <form method="POST" action="<c:url value="/user/registration/accept"/>">
            <table class="order-fields">
                <tbody>
                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userName">
                                Имя<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input class="elem input-elem" id="userName" name="user_name" type="text" value=""/>
                        </td>
                    </tr>
                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userEmail">
                                Электронная почта<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input class="elem input-elem" id="userEmail" name="user_email" type="text" value=""/>
                        </td>
                    </tr>
                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userPass">
                                Пароль<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input class="elem input-elem" id="userPass" name="user_pass" type="text" value=""/>
                        </td>
                    </tr>
                    <tr class="order-field">
                        <td class="iLabel">
                            <label for="userPass2">
                                Повторите пароль<span class="imp">*</span>
                            </label>
                        </td>
                        <td class="iField">
                            <input class="elem input-elem" id="userPass2" name="user_pass2" type="text" value=""/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <button class="btn ibutton-orange ibutton-reg-submit" name="acceptOrder" type="submit">
                                Зарегистрироваться
                            </button>
                        </td>
                        <td>
                            <button class="btn ibutton-grey ibutton-reg-reset" name="resetOrder" type="button" onclick="eventHandler.goToReferrerUrl()">
                                Отмена
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</div>
