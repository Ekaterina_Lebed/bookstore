
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--%@ page pageEncoding="UTF-8" %-->

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>

<html>
<head>
    <title>My books</title>
    <meta charset="UTF-8">

    <script type="text/javascript" src="<c:url value="/resources/script/handler/PageEventHandler.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/script/handler/CartEventHandler.js"/>"></script>

    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/main.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/book-object.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/book-list-top.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/catalog.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/cart.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/order.css"/>"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/office.css"/>"/>
    </head>

<body>
    <div class="wrapper-body">
        <center>
            <div class="wrapper-body1200">
                <tiles:insertAttribute name="topheader"/>
                <tiles:insertAttribute name="header"/>
                <tiles:insertAttribute name="menu" />

                <div class="page-content">
                    <tiles:insertAttribute name="body"/>
                </div>
                <tiles:insertAttribute name="footer" />
            </div>
        </center>
    </div>
    <div id="GOWrapper" class="">
        <div id="GOWrapperArrow" onclick="eventHandler.goToBottomPage()">Наверх</div>
    </div>
</body>
</html>