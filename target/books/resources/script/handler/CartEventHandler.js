
var cartEventHandler = (function() {

    function cartRequestModule(){
        var module={};

        function selectCartItem(cartState,id){
            if(id==null)return null;
            for (var index in cartState.cartItems){
                var cartItem=cartState.cartItems[index];
                if(String(cartItem.id)==String(id)){
                    return cartItem;
                }
            }
            return null;
        }

        function doRequestGET(path,idGood){
            var resp=null;
            var xhr = new XMLHttpRequest();
            xhr.open('GET', path);
            xhr.send();
            xhr.onreadystatechange = function() {
                if (xhr.readyState != 4) {
                    return;
                }

                var cartState=JSON.parse(xhr.responseText);

                resp={};
                resp.cartState=cartState;
                resp.cartItem=selectCartItem(cartState,idGood);

                cartView.update(resp);
            }
        }

        module.updateCount = function (idGood,count){
            if(isNaN(count)||count<1) count=1;
            else if(count>100)count=100;

            var path='/books/cart/update?id='+idGood+'&count='+count;
            doRequestGET(path,idGood);
        };

        module.addItem=function (idGood){
            var path='/books/cart/add/?id='+idGood;
            doRequestGET(path);
        };

        module.deleteItem=function(idGood){
            var path='/books/cart/delete?id='+idGood;
            doRequestGET(path);
        };

        module.getCart=function(){
            var path='/books/cart';
            doRequestGET(path);
        };

        return module;
    }

    function cartFormatModule(){
        var module={};
        module.declOfNum=function declOfNum(number, titles){
            var cases = [2, 0, 1, 1, 1, 2];
            return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];
        };

        module.declOfNumForGoodsSeconds=function declOfNumForGoodsSeconds(i){
            var titles=['секунду', 'секунды', 'секунд'];
            return declOfNum (i,titles);
        };

        module.declOfNumForGoods=function declOfNumForGoods(i){
            var titles=['товар', 'товара', 'товаров'];
            return this.declOfNum(i,titles);
        };

        return module;
    }

    function cartViewModule(){
        var module={};

        function getCartItemsElement(){
            return document.getElementById("cart-items");
        }

        function getCartItemsElements(){
            return document.getElementsByClassName("cart-item");
        }

        function getCartStateSumElement(){
            return document.getElementById('totalSum');
        }

        function getCartItemsId(cartState){
            var result=[];
            var cartItem;
            for (var ind in cartState.cartItems){
                cartItem=cartState.cartItems[ind];
                result[result.length] ="cartItem["+cartItem.id+"]";
            }
            return result;
        }


        function checkExistingOfCartItems(cartState, isExistParam){
            var ElementsSet=getCartItemsElements();
            var idSet=getCartItemsId(cartState);

            var result=[];
            for (var index = 0; index < ElementsSet.length; index++){
                var elementNode=ElementsSet[index];
                var isExist=false;
                for(var ind in idSet){
                    if(elementNode.id==idSet[ind]){
                        isExist=true;
                        break;
                    }
                }

                if(isExist==isExistParam){
                    result[result.length]=elementNode;
                }
            }
            return result;
        }

        function removeCartItems(itemsElements){
            var cartElement=getCartItemsElement();
            if(cartElement==null)return;

            for(var i=0; i< itemsElements.length; i++){
                cartElement.removeChild(itemsElements[i]);
            }
        }


        module.getItemCount=function(idGood){
            var elementCount=document.getElementById('itemCount['+idGood+']');
            if(elementCount!=null) {
                return Number(elementCount.value);
            }
            return NaN;
        };

        module.updateSelectedItemView=function(cartItem){
            if(cartItem==null)return;

            var elementCount=document.getElementById('itemCount['+cartItem.id+']');
            var elementSum=document.getElementById('itemSum['+cartItem.id+']');

            if(elementCount!=null)elementCount.value=cartItem.count.toFixed(0);
            if(elementSum!=null)elementSum.innerHTML=cartItem.sum.toFixed(2);
        };

        module.updateItemsView=function(cartState){
            var cartItemsElement=getCartItemsElement();
            if(cartItemsElement==null)return;

            var itemsToRemove=checkExistingOfCartItems(cartState, false);
            removeCartItems(itemsToRemove);

            var elementSum=getCartStateSumElement();
            if(elementSum!=null)elementSum.innerHTML=cartState.sum.toFixed(2);
        };

        module.updateHeaderView=function(cartState){
            document.getElementById("cartGoods").innerHTML =
                cartState.count.toFixed(0)+" "+formatHandler.declOfNumForGoods(cartState.count);
            document.getElementById("cartSum").innerHTML =
                cartState.sum.toFixed(2)+" "+cartState.currency.name;
        };

        module.update=function(resp){
            cartView.updateHeaderView(resp.cartState);
            cartView.updateItemsView(resp.cartState);
            cartView.updateSelectedItemView(resp.cartItem);
        };

        return module;
    }

    var cartRequest = cartRequestModule();

    var formatHandler = cartFormatModule();
    var cartView = cartViewModule();

    var module = {};

    module.addItem = function(idGood){
        cartRequest.addItem(idGood);
    };

    module.deleteItem = function(idGood){
        cartRequest.deleteItem(idGood);
    };

    module.update = function(){
        cartRequest.getCart();
    };

    module.setCount = function(idGood){
        var count = cartView.getItemCount(idGood);
        cartRequest.updateCount(idGood,count);
    };

    module.increaseCount = function(idGood){
        var count = cartView.getItemCount(idGood)+1;
        cartRequest.updateCount(idGood,count);
    };

    module.reduceCount = function(idGood){
        var count = cartView.getItemCount(idGood)-1;
        cartRequest.updateCount(idGood,count);
    };

    return module;

})();