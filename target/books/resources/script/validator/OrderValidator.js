
var OrderValidator = (function() {

    function showError(formElement,isValid,errMessage) {
        var childNodes = formElement.parentNode.childNodes;

        for(var i = 0; i < childNodes.length; i++){
            var elementNode = childNodes.item(i);

            if(elementNode.className == "error-info"){
                elementNode.innerHTML = (isValid) ? "" : errMessage;
            }
        }
    }

    var module = {};

    module.validate = function (){
        var elementField;
        var isValid;
        var result = true;

        elementField = document.getElementById("orderUserName");
        isValid = ValueValidator.isUserFullNameValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнено имя");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderPhone");
        isValid = ValueValidator.isPhoneNumberValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнен телефон");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderEmail");
        isValid = ValueValidator.isEmailValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнен e-mail");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderCity");
        isValid = ValueValidator.isCityNameValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнен город");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderStreet");
        isValid = ValueValidator.isEntityNameValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнена улица");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderHouse");
        isValid = ValueValidator.isHouseNumberValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнен дом");
        result = (!isValid) ? isValid : result;

        elementField = document.getElementById("orderFlat");
        isValid = ValueValidator.isHouseNumberValid(elementField.value);
        showError(elementField,isValid,"Не верно заполнена квартира");
        result = (!isValid) ? isValid : result;

        return result;
    };

    return module;

})();
