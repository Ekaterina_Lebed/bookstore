
var ValueValidator = (function() {

    var module = {};

    module.isUserLoginValid = function (value){
        var regExp = /^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$/i;

        return regExp.test(value);
    };

    module.isUserFullNameValid = function (value){
        var regExp = /^[а-яА-ЯёЁa-zA-Z0-9\s]+$/i;

        return regExp.test(value);
    };

    module.isCityNameValid = function (value){
        var regExp = /^[а-яА-ЯёЁa-zA-Z\s]+$/i;

        return regExp.test(value);
    };

    module.isEntityNameValid = function (value){
        var regExp = /^[а-яА-ЯёЁa-zA-Z0-9\s]+$/i;

        return regExp.test(value);
    };

    module.isPhoneNumberValid = function (value){
        var regExp = /\([0-9]{3}\)\s[0-9]{3}-[0-9]{2}-[0-9]{2}$/;
        var regExp2 = /^\+\d{2}\(\d{3}\)\d{3}-\d{2}-\d{2}$/;

        return regExp.test(value) || regExp2.test(value);
    };

    module.isEmailValid = function (value){
        var regExp = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/i;

        return regExp.test(value);
    };

    module.isHouseNumberValid=function (value){
        var regExp2 =/^[0-9]{1,3}[\s-]{1}$/i;
        if(regExp2.test(value)){
            return false;
        }

        var regExp = /^[0-9]{1,3}[\s-]{0,1}[а-яА-Я]{0,1}$/i;
        return regExp.test(value);
    };

    module.isIntegerNumberValid=function (value){
        var regExp = /^[1-9]\d*$/;

        return regExp.test(value);
    };

    module.isHttpsValid=function (value){
        var regExp=/^((https?|ftp)\:\/\/)?([a-z0-9]{1})((\.[a-z0-9-])|([a-z0-9-]))*\.([a-z]{2,6})(\/?)$/;

        return regExp.test(value);
    };

    return module;
})();