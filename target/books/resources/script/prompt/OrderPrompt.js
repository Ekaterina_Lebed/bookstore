
var OrderPrompt = (function() {

    function showPrompt(elementName,promptText){
        var element = document.getElementById(elementName);
        element.setAttribute("placeholder",promptText);
    }

    var module = {};

    module.showPrompts = function (){
        showPrompt("orderPhone","(XXX) XXX-XX-XX");
        showPrompt("orderStreet","улица");
        showPrompt("orderHouse","дом");
        showPrompt("orderFlat","квартира");
        showPrompt("orderUserName","имя");
        showPrompt("orderUserLastName","фамилия");
    };

    return module;
})();
